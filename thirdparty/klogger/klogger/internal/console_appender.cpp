/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstring>
#include <iostream>
#include <vector>

#include "../../..//src/util/string_util.hh"
#include "color.h"
#include "console_appender.h"
#include "logger_impl.h"

ConsoleAppender::ConsoleAppender(LoggerImpl *impl, const std::string &name,
                                 const std::string &attribute)
    : _name("[" + name + "]"), _realName(name), _flush(false), _impl(impl),
      _level(Logger::MIN) {
  // 获取配置
  parseAttribute(attribute);
}

ConsoleAppender::~ConsoleAppender() {}

void ConsoleAppender::write(int level, const char *format, ...) {
  if (level < _level) {
    return;
  }
  va_list va_ptr;
  va_start(va_ptr, format);
  if (level == Logger::WARNING) {
    std::cout << blue;
  } else if (level == Logger::EXCEPTION) {
    std::cout << green;
  } else if (level == Logger::FAILURE) {
    std::cout << yellow;
  } else if (level == Logger::FATAL) {
    std::cout << red;
  } else if (level == Logger::DIAGNOSE) {
    std::cout << blue;
  }
  write(getLevelString(level), format, va_ptr);
  std::cout << white;
  va_end(va_ptr);
}

void ConsoleAppender::setLevel(int level) { _level = level; }

int ConsoleAppender::getLevel() { return _level; }

void ConsoleAppender::destroy() {
  // 从管理器内删除
  _impl->remove(_realName);
  // 销毁自己
  delete this;
}

void ConsoleAppender::write(const std::string &level, const char *format,
                            va_list va_ptr) {
  std::string pattern = _pattern.getPrefix();
  int len = 0;
  char buffer[64 * 1024] = {0};
  if (!level.empty()) { // 日志等级
    memcpy(buffer + len, level.c_str(), (int)level.size());
    len += (int)level.size();
  }
  if (!_name.empty()) { // 日志名称
    memcpy(buffer + len, _name.c_str(), (int)_name.size());
    len += (int)_name.size();
  }
  if (!pattern.empty()) { // 日志行前缀
    memcpy(buffer + len, pattern.c_str(), (int)pattern.size());
    len += (int)pattern.size();
  }
  int bytes = vsnprintf(buffer + len, sizeof(buffer) - len, format, va_ptr);
  if (0 >= bytes) {
    return;
  }
  len += bytes;
  ScopeLock<RecursiveLock> guard(&_lock);
  fprintf(stderr, "%s%s", buffer, LINE_END);
  if (_flush) {
    fflush(stderr);
  }
}

void ConsoleAppender::reload(const std::string &attribute) {
  parseAttribute(attribute);
}

void ConsoleAppender::parseAttribute(const std::string &attribute) {
  std::vector<std::string> tokens;
  kratos::util::split(attribute, ";", tokens);
  std::vector<std::string>::iterator guard = tokens.begin();
  // 解析日志属性配置
  for (; guard != tokens.end(); guard++) {
    if (std::string::npos != guard->find("line=")) {
      _line = getAttribute("line=", *guard);
      _pattern.parse(_line);
    } else if (std::string::npos != guard->find("flush=")) {
      std::string flush = getAttribute("flush=", *guard);
      _flush = (flush == "true");
    } else if (std::string::npos != guard->find("level=")) {
      std::string level = getAttribute("level=", *guard);
      _level = atoi(level.c_str());
    }
  }
}

std::string ConsoleAppender::getAttribute(const std::string &name,
                                          const std::string &attribute) {
  size_t pos = attribute.find(name);
  if (pos == std::string::npos) {
    throw LoggerException("invalid attribute");
  }
  return attribute.substr(pos + name.size());
}

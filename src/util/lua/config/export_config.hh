#pragma once

#include "../lua_export_class.h"
#include <string>

namespace kratos {
namespace service {
class ServiceBox;
}
} // namespace kratos

namespace kratos {
namespace config {
class BoxConfigImpl;
class BoxConfig;
} // namespace config
} // namespace kratos

namespace kratos {
namespace lua {

class LuaServiceImpl;

#define DECL_LUA_METHOD(name) static int lua_##name(lua_State *l);

/**
 * @brief 配置器lua导出包装
 */
class LuaConfig : public LuaExportClass {
  kratos::service::ServiceBox *box_{nullptr}; ///< 服务容器
  lua_State *L_{nullptr};                     ///< lua虚拟机
  int lua_reg_key_{LUA_NOREF};                ///< Lua注册表key
  using ListenerNameList = kratos::service::PoolList<std::string>;
  ListenerNameList listener_name_list_; ///< 重载监听器名字
  bool reloaded_{false};                ///< 是否需要重载
  LuaServiceImpl *service_{nullptr};    ///< lua服务

public:
  /**
   * @brief 构造
   * @param box 服务容器
   * @param L 虚拟机
   * @param service lua service
   */
  LuaConfig(kratos::service::ServiceBox *box, lua_State *L,
            LuaServiceImpl *service);
  /**
   * 析构
   */
  virtual ~LuaConfig();
  virtual auto do_register() -> bool override;
  virtual auto update(std::time_t ms) -> void override;
  virtual auto do_cleanup() -> void override;

private:
  DECL_LUA_METHOD(get_listener_list);
  DECL_LUA_METHOD(get_service_finder_type);
  DECL_LUA_METHOD(get_service_finder_hosts);
  DECL_LUA_METHOD(get_service_finder_connect_timeout);
  DECL_LUA_METHOD(get_necessary_service);
  DECL_LUA_METHOD(get_connect_other_box_timeout);
  DECL_LUA_METHOD(get_box_channel_recv_buffer_len);
  DECL_LUA_METHOD(get_box_name);
  DECL_LUA_METHOD(get_logger_config_line);
  DECL_LUA_METHOD(get_service_dir);
  DECL_LUA_METHOD(get_preload_service);
  DECL_LUA_METHOD(is_open_coroutine);
  DECL_LUA_METHOD(get_remote_service_repo_version_api);
  DECL_LUA_METHOD(get_remote_service_repo_dir);
  DECL_LUA_METHOD(get_remote_service_repo_latest_version_api);
  DECL_LUA_METHOD(is_open_remote_update);
  DECL_LUA_METHOD(get_remote_repo_check_interval);
  DECL_LUA_METHOD(is_start_as_daemon);
  DECL_LUA_METHOD(get_http_max_call_timeout);
  DECL_LUA_METHOD(is_open_rpc_stat);

  DECL_LUA_METHOD(has_attribute);
  DECL_LUA_METHOD(get_array);
  DECL_LUA_METHOD(get_table);
  DECL_LUA_METHOD(get_string);
  DECL_LUA_METHOD(get_number);
  DECL_LUA_METHOD(get_integer);
  DECL_LUA_METHOD(add_reload_listener);
  DECL_LUA_METHOD(remove_reload_listener);

  auto reload_listener(const std::string &, const kratos::config::BoxConfig &)
      -> void;
};

} // namespace lua
} // namespace kratos

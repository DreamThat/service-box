#pragma once

#include <cstdint>
#include <ctime>
#include <functional>
#include <string>

namespace kratos {
namespace service {

class Command;

/**
 * 命令处理器
 * 参数为命令，返回值为执行结果
 */
using CommandProc = std::function<auto(const std::string &)->std::string>;

/**
 * 注册并执行外部发送的命令
 */
class Command {
public:
  virtual ~Command() {}
  /**
   * 注册命令处理器
   *
   * \param name 命令名
   * \param max_exec_time 最大执行超时时间，毫秒
   * \param proc 命令处理器
   * \return true或false
   */
  virtual auto wait_for(
    const std::string &name,
    std::time_t max_exec_time,
    CommandProc proc) -> bool = 0;
  /**
   * 注册目录处理器
   *
   * \param path 目录
   * \param max_exec_time 最大执行超时时间，毫秒
   * \param proc 命令处理器
   * \return true或false
   */
  virtual auto wait_path(
    const std::string& path,
    std::time_t max_exec_time,
    CommandProc proc) -> bool = 0;
};

} // namespace service
} // namespace kratos

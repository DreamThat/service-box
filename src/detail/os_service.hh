#pragma once

#include <string>

namespace kratos {
namespace service {

class ServiceBox;

/**
 * 服务控制状态.
 */
enum class OSCtrlStatus {
    OK = 1, ///< 启动成功
    FAIL, ///< 启动失败
    PASS, ///< 以非服务模式启动
};

/**
 * 操作系统服务.
 */
class OperatingSystemServiceCtrl {
 public:
  /**
   * 构造.
   * 
   */
  OperatingSystemServiceCtrl();
  /**
   * 析构.
   *
   */
  ~OperatingSystemServiceCtrl();

  /**
   * 检查是否以服务模式启动.
   * 
   * \param argc 参数个数
   * \param argv 参数数组
   * \return 控制状态
   */
  auto check_and_run(int argc, char **argv) -> OSCtrlStatus;
};

} // namespace service
} // namespace kratos

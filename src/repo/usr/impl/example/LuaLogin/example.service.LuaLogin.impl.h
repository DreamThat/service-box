#pragma once

#include "example.service.LuaLogin.h"
#include "service_context.hh"

class LuaLoginImpl : public LuaLogin {
    kratos::service::LuaServicePtr lua_service_;
public:
//implementation->
    LuaLoginImpl();
    virtual ~LuaLoginImpl();
    virtual bool onAfterFork(rpc::Rpc* rpc) override;
    virtual bool onBeforeDestory(rpc::Rpc* rpc) override;
    virtual void onTick(std::time_t ms) override;
    virtual void onServiceCall(rpc::StubCallPtr callPtr) override;
//implementation<-
};


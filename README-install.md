# 安装依赖

## Windows

需要首先安装JDK8.

下载
```
https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
```
配置环境路径
```
https://www.cnblogs.com/liuhongfeng/p/4177568.html
```

安装cmake版本>3.5

下载
```
https://cmake.org/download/
```

安装Visual Studio Community 2019

下载
```
https://visualstudio.microsoft.com/zh-hans/vs/
```

1. 搭建单机运行环境

使用管理员权限运行如下命令:

```
cd build
install_win.bat
```

2. 单机环境启动/关闭

启动

```
tools目录下运行: start_win_local_env.bat
```

关闭
```
tools目录下运行: stop_win_local_env.bat
```

3. 发布

[发布默认容器](https://gitee.com/dennis-kk/service-box/tree/master/publish)

4. 启动默认容器

启动

```
tools目录下运行: start_win_box.bat
```

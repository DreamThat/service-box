/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_APPENDER_H
#define CONSOLE_APPENDER_H

#include <cstdarg>
#include <cstdio>
#include <list>
#include <string>

#include "../interface/logger.h"
#include "mode.h"
#include "scope_lock.h"
#include "util.h"

class LoggerImpl;

using namespace klogger;

class ConsoleAppender : public Appender {
public:
  ConsoleAppender(LoggerImpl *impl, const std::string &name,
                  const std::string &attribute);
  virtual ~ConsoleAppender();
  virtual void write(int level, const char *format, ...);
  virtual void setLevel(int level);
  virtual int getLevel();
  virtual void destroy();
  virtual void reload(const std::string &attribute);

private:
  /**
   * 解析配置字符串
   * @param attribute 配置字符串
   */
  void parseAttribute(const std::string &attribute);

  /**
   * 取得配置值
   * @param name 配置键名称
   * @param attribute 配置
   * @return 配置值
   */
  std::string getAttribute(const std::string &name,
                           const std::string &attribute);

  /**
   * 写日志行
   * @param level 日志等级字符串
   * @param format 日志格式
   * @param va_ptr 可变参数宏
   */
  void write(const std::string &level, const char *format, va_list va_ptr);

private:
  std::string _line;     ///< 日志行前缀
  std::string _name;     ///< 添加器在日志行中显示的名称
  std::string _realName; ///< 添加器名称
  bool _flush;           ///< flush标志
  LoggerImpl *_impl;     ///< Logger实现类
  Pattern _pattern;      ///< 日志行前缀匹配器
  RecursiveLock _lock;   ///< 锁
  int _level;            ///< 日志等级
};

#endif // CONSOLE_APPENDER_H

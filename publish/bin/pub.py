#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import os
import getopt
import platform
import shutil
import json
import glob

global cwd
cwd = os.getcwd()
global abs_path
abs_path = os.path.abspath(os.path.dirname(__file__)) 
if cwd != abs_path:
    os.chdir(abs_path)

class CxxPublisher:
    def __init__(self, out_dir, remote_repo, name, config_file, no_client):
        self.out_dir = out_dir
        self.remote_repo = remote_repo
        self.name = name
        self.config_file = config_file
        self.no_client = no_client
        self.config = None
        self.proxy_all = False
        self.stub_all = False
        self.root_dir,_ = os.path.split(config_file)
        if self.name is None:
            self.name = 'service_box'
        if self.config_file is not None:
            self.config = json.load(open(config_file))
            if self.config.has_key("binary_name"):
                self.name = self.config["binary_name"]
        if not self.config.has_key("proxy"):
            self.proxy_all = True
        if not self.config.has_key("bundle"):
            self.stub_all = True

    def get_config_file(self):
        if self.config is None:
            return None
        if self.config.has_key("config"):
            return self.config["config"]
        return None

    def is_need_proxy(self, idl_name, name):
        if self.config is None:
            return True
        if not self.config.has_key("proxy"):
            return True
        for info in self.config["proxy"]:
            if info.has_key("name") and info.has_key("proxy"):
                if info["name"] == idl_name and "lib" + info["proxy"] + "_proxy.a" == name:
                    return True
        return False

    def is_need_stub(self, idl_name, name):
        if self.config is None:
            return True
        if not self.config.has_key("bundle"):
            return True
        for info in self.config["bundle"]:
            if info.has_key("name") and info.has_key("service"):
                if info["name"] == idl_name and "lib" + info["proxy"] + "_stub.a" == name:
                    return True
        return False

    def is_need_service(self, idl_name):
        if self.proxy_all or self.stub_all:
            return True
        if self.config is None:
            return True
        if not self.config.has_key("bundle") and not self.config.has_key("proxy"):
            return True
        if self.config.has_key("proxy"):
            for info in self.config["proxy"]:
                if info.has_key("name"):
                    if info["name"] == idl_name:
                        return True
        if self.config.has_key("bundle"):
            for info in self.config["bundle"]:
                if info.has_key("name"):
                    if info["name"] == idl_name:
                        return True
        return False

    def is_need_bundle(self, name):
        if self.config is None:
            return True
        if not self.config.has_key("bundle"):
            return True
        for info in self.config["bundle"]:
            if info.has_key("name") and info.has_key("service"):
                if info["name"] == idl_name and "lib" + info["proxy"] + ".so" == name:
                    return True
        return False

    def recursive_overwrite(self, src, dest, ignore=None):
        if os.path.isdir(src):
            if not os.path.isdir(dest):
                os.makedirs(dest)
            files = os.listdir(src)
            if ignore is not None:
                ignored = ignore(src, files)
            else:
                ignored = set()
            for f in files:
                if f not in ignored:
                    self.recursive_overwrite(os.path.join(src, f), 
                                        os.path.join(dest, f), 
                                        ignore)
        else:
            shutil.copyfile(src, dest)
           
    def do_publish_client(self):
        cmake = open("CMakeLists.txt", "w")
        cmake.write('cmake_minimum_required(VERSION 3.5)\n')
        cmake.write('project(box_client)\n')
        cmake.write('IF (MSVC)\n')
        cmake.write('  set(CMAKE_PREFIX_PATH "C:/Program Files (x86)/protobuf" "${CMAKE_PREFIX_PATH}")\n')
        cmake.write('  set(PROTOBUF_PATH "C:/Program Files (x86)/protobuf")\n')
        cmake.write('  find_package(Protobuf REQUIRED)\n')
        cmake.write('  include_directories(${Protobuf_INCLUDE_DIRS})\n')
        cmake.write('ELSE()\n')
        cmake.write('  INCLUDE_DIRECTORIES(/usr/include/)\n')
        cmake.write('  SET(CMAKE_CXX_FLAGS "-m64 -g -O2 -std=c++17 -DUSE_STATIC_LIB -DTHREADED -DCURL_STATICLIB -Wall -Wno-unused-variable -fnon-call-exceptions")\n')
        cmake.write('  set(CMAKE_CXX_COMPILER "g++")\n')
        cmake.write('  link_directories(/usr/local/lib)\n')
        cmake.write('ENDIF()\n')
        cmake.write('SET(EXECUTABLE_OUTPUT_PATH ' + self.out_dir + ')\n')
        cmake.write('INCLUDE_DIRECTORIES(\n')
        cmake.write(' ${PROJECT_SOURCE_DIR}/../../src/repo/src/include/root\n')
        cmake.write(' ${PROJECT_SOURCE_DIR}/../../src/repo/src/include/example\n')
        cmake.write(')\n')
        cmake.write('IF (MSVC)\n')
        cmake.write('foreach(var\n')
        cmake.write('  CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE\n')
        cmake.write('  CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO\n')
        cmake.write('  CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE\n')
        cmake.write('  CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO\n')
        cmake.write(')\n')
        cmake.write('if(${var} MATCHES "/MD")\n')
        cmake.write('  string(REGEX REPLACE "/MD" "/MT" ${var} "${${var}}")\n')
        cmake.write('endif()\n')
        cmake.write('endforeach()\n')
        cmake.write('  add_executable (box_client\n')
        cmake.write('      ${PROJECT_SOURCE_DIR}/../../src/lib/lib_main.cpp\n')
        cmake.write('  )\n')
        cmake.write('  set_target_properties(box_client PROPERTIES COMPILE_FLAGS "/EHsc /W3")\n')
        cmake.write('  INCLUDE_DIRECTORIES(\n')
        cmake.write('      ../../thirdparty/curl/include\n')
        cmake.write('      ../../thirdparty/zookeeper/include\n')
        cmake.write('      ../../thirdparty/hiredis/include\n')
        cmake.write('  )\n')
        # 遍历所有服务
        service_dir_list = []
        service_name_list = []
        for dir in os.listdir("../../src/repo/lib/"):
            if (dir != 'root') and (dir != 'proxy') and (dir != 'stub'):
                service_dir_list.append('${PROJECT_SOURCE_DIR}/../../src/repo/lib/' + dir + '/')
                service_name_list.append(dir + '.a')
        proxy_dir_list = []
        proxy_name_list = []
        for dir_name in os.listdir("../../src/repo/lib/proxy"):
            if not os.path.isdir("../../src/repo/lib/proxy/" + dir_name):
                continue;
            for proxy_sub_dir in os.listdir("../../src/repo/lib/proxy/" + dir_name):
                proxy_lib_path = "${PROJECT_SOURCE_DIR}/../../src/repo/lib/proxy/" + dir_name + '/' + proxy_sub_dir
                proxy_dir_list.append(proxy_lib_path + '/')
                proxy_name_list.append('lib' + proxy_sub_dir + '_proxy.a')
        stub_dir_list_debug = []
        stub_dir_list_release = []
        stub_so_dir_list_debug = []
        stub_so_dir_list_release = []
        stub_so_name_win = []
        for dir_name in os.listdir("../../src/repo/lib/stub"):
            if not os.path.isdir("../../src/repo/lib/stub/" + dir_name):
                continue;
            temp_name = "../../src/repo/lib/stub/" + dir_name
            for stub_sub_dir in os.listdir(temp_name):
                stub_lib_path = "${PROJECT_SOURCE_DIR}/../../src/repo/lib/stub/" + dir_name + '/' + stub_sub_dir
                stub_dir_list_debug.append(stub_lib_path + '/Debug/' + 'lib' + stub_sub_dir + '_stub.a')
                stub_dir_list_release.append(stub_lib_path + '/Release/' + 'lib' + stub_sub_dir + '_stub.a')
                abs_service_path = abs_path + '/../../src/repo/lib/stub/' + dir_name + '/' + stub_sub_dir + '/Debug/' + 'lib' + stub_sub_dir + '.a'
                if os.path.exists(abs_service_path):
                    stub_dir_list_debug.append(stub_lib_path + '/Debug/' + 'lib' + stub_sub_dir + '.a')
                    stub_dir_list_release.append(stub_lib_path + '/Release/' + 'lib' + stub_sub_dir + '.a')
                abs_so_path = abs_path + '/../../src/repo/lib/stub/' + dir_name + '/' + stub_sub_dir + '/Debug/' + 'lib' + stub_sub_dir + '.so'
                if os.path.exists(abs_so_path):
                    stub_so_dir_list_debug.append(abs_path + '/../../src/repo/lib/stub/' + dir_name + '/' + stub_sub_dir + '/Debug/' + 'lib' + stub_sub_dir + '.so')
                    stub_so_dir_list_release.append(abs_path + '/../../src/repo/lib/stub/' + dir_name + '/' + stub_sub_dir + '/Release/' + 'lib' + stub_sub_dir + '.so')
                    stub_so_name_win.append('lib' + stub_sub_dir + '.so')
        cmake.write('  SET_TARGET_PROPERTIES(box_client PROPERTIES LINK_FLAGS_DEBUG \"')
        for dir, name in zip(service_dir_list, service_name_list):
            cmake.write('/WHOLEARCHIVE:' + dir + "Debug/lib" + name)
        for dir, name in zip(proxy_dir_list, proxy_name_list):
            cmake.write(' /WHOLEARCHIVE:' + dir + "Debug/" + name)
        for path in stub_dir_list_debug:
            cmake.write(' /WHOLEARCHIVE:' + path)
        cmake.write('\")\n')
        cmake.write('  SET_TARGET_PROPERTIES(box_client PROPERTIES LINK_FLAGS_RELEASE \"')
        for dir, name in zip(service_dir_list, service_name_list):
            cmake.write('/WHOLEARCHIVE:' + dir + "Release/lib" + name)
        for dir, name in zip(proxy_dir_list, proxy_name_list):
            cmake.write(' /WHOLEARCHIVE:' + dir + "Release/" + name)
        for name in stub_dir_list_release:
            cmake.write(' /WHOLEARCHIVE:' + name)
        cmake.write('\")\n')
        cmake.write('  target_link_libraries(box_client\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../win-proj/x64/Debug/service-box.lib optimized ${PROJECT_SOURCE_DIR}/../../win-proj/x64/Release/service-box.lib\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../src/repo/lib/root/Debug/librpc.a optimized ${PROJECT_SOURCE_DIR}/../../src/repo/lib/root/Release/librpc.a\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../thirdparty/curl/Debug/libcurl.a optimized ${PROJECT_SOURCE_DIR}/../../thirdparty/curl/Release/libcurl.a\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../thirdparty/zookeeper/Debug/zookeeper.lib optimized ${PROJECT_SOURCE_DIR}/../../thirdparty/zookeeper/Release/zookeeper.lib\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../thirdparty/zookeeper/Debug/hashtable.lib optimized ${PROJECT_SOURCE_DIR}/../../thirdparty/zookeeper/Release/hashtable.lib\n')
        cmake.write('    debug ${PROTOBUF_PATH}/lib/libprotobufd.lib optimized ${PROTOBUF_PATH}/lib/libprotobuf.lib\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../thirdparty/hiredis/lib/Debug/hiredis.lib optimized ${PROJECT_SOURCE_DIR}/../../thirdparty/hiredis/lib/Release/hiredis.lib\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../thirdparty/hiredis/lib/Debug/Win32_Interop.lib optimized ${PROJECT_SOURCE_DIR}/../../thirdparty/hiredis/lib/Release/Win32_Interop.lib\n')
        cmake.write('    wldap32.lib\n')
        cmake.write('    ws2_32.lib\n')
        cmake.write('    psapi.lib\n')
        cmake.write('  )\n')
        cmake.write('ELSE()\n')
        cmake.write('add_executable (box_client ${PROJECT_SOURCE_DIR}/../../src/lib/lib_main.cpp)\n')
        cmake.write('  target_link_libraries(box_client\n')
        cmake.write('    ${PROJECT_SOURCE_DIR}/../../src/lib/libservice_box.a\n')
        cmake.write('    ${PROJECT_SOURCE_DIR}/../../src/repo/lib/root/librpc.a\n')
        cmake.write('    -lprotobuf\n')
        cmake.write('    libzookeeper.a\n')
        cmake.write('    libhashtable.a\n')
        cmake.write('    -lcurl\n')
        cmake.write('    -lpthread\n')
        cmake.write('    -ldl\n')
        cmake.write('    -lhiredis\n')
        cmake.write('  )\n')
        cmake.write('ENDIF()\n')
        cmake.close()
        if platform.system() == "Windows":
            os.system('cmake -G  "Visual Studio 16 2019" -A x64')
            os.system('cmake --build . --config "Release"')
            os.system('cmake --build . --config "Debug"')
        else:
            os.system("cmake .")
            os.system("make")

    def do_publish(self):
        cmake = open("CMakeLists.txt", "w")
        cmake.write('cmake_minimum_required(VERSION 3.5)\n')
        cmake.write('project(' + self.name + ')\n')
        cmake.write('IF (MSVC)\n')
        cmake.write('  set(CMAKE_PREFIX_PATH "C:/Program Files (x86)/protobuf" "${CMAKE_PREFIX_PATH}")\n')
        cmake.write('  set(PROTOBUF_PATH "C:/Program Files (x86)/protobuf")\n')
        cmake.write('  find_package(Protobuf REQUIRED)\n')
        cmake.write('  include_directories(${Protobuf_INCLUDE_DIRS})\n')
        cmake.write('ELSE()\n')
        cmake.write('  INCLUDE_DIRECTORIES(/usr/include/)\n')
        cmake.write('  SET(CMAKE_CXX_FLAGS "-m64 -g -O2 -std=c++17 -DUSE_STATIC_LIB -DTHREADED -DCURL_STATICLIB -Wall -Wno-unused-variable -fnon-call-exceptions")\n')
        cmake.write('  set(CMAKE_CXX_COMPILER "g++")\n')
        cmake.write('  link_directories(/usr/local/lib)\n')
        cmake.write('ENDIF()\n')
        cmake.write('SET(EXECUTABLE_OUTPUT_PATH ' + self.out_dir + "/" + self.name + ')\n')
        cmake.write('INCLUDE_DIRECTORIES(\n')
        cmake.write(' ${PROJECT_SOURCE_DIR}/../../src/repo/src/include/root\n')
        cmake.write(' ${PROJECT_SOURCE_DIR}/../../src/repo/src/include/example\n')
        cmake.write(')\n')
        cmake.write('IF (MSVC)\n')
        cmake.write('foreach(var\n')
        cmake.write('  CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE\n')
        cmake.write('  CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO\n')
        cmake.write('  CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE\n')
        cmake.write('  CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO\n')
        cmake.write(')\n')
        cmake.write('if(${var} MATCHES "/MD")\n')
        cmake.write('  string(REGEX REPLACE "/MD" "/MT" ${var} "${${var}}")\n')
        cmake.write('endif()\n')
        cmake.write('endforeach()\n')
        cmake.write('  add_executable (' + self.name + '\n')
        cmake.write('      ${PROJECT_SOURCE_DIR}/../../src/lib/lib_main.cpp\n')
        cmake.write('  )\n')
        cmake.write('  set_target_properties(' + self.name +  ' PROPERTIES COMPILE_FLAGS "/EHsc /W3")\n')
        cmake.write('  INCLUDE_DIRECTORIES(\n')
        cmake.write('      ../../thirdparty/curl/include\n')
        cmake.write('      ../../thirdparty/zookeeper/include\n')
        cmake.write('      ../../thirdparty/hiredis/include\n')
        cmake.write('  )\n')
        # 遍历所有服务
        service_dir_list = []
        service_name_list = []
        for dir in os.listdir("../../src/repo/lib/"):
            if (dir != 'root') and (dir != 'proxy') and (dir != 'stub'):
                if self.is_need_service(dir):
                    service_dir_list.append('${PROJECT_SOURCE_DIR}/../../src/repo/lib/' + dir + '/')
                    service_name_list.append(dir + '.a')
        proxy_dir_list = []
        proxy_name_list = []
        for dir_name in os.listdir("../../src/repo/lib/proxy"):
            if not os.path.isdir("../../src/repo/lib/proxy/" + dir_name):
                continue;
            for proxy_sub_dir in os.listdir("../../src/repo/lib/proxy/" + dir_name):
                proxy_lib_path = "${PROJECT_SOURCE_DIR}/../../src/repo/lib/proxy/" + dir_name + '/' + proxy_sub_dir
                proxy_dir_list.append(proxy_lib_path + '/')
                proxy_name_list.append('lib' + proxy_sub_dir + '_proxy.a')
        stub_dir_list_debug = []
        stub_dir_list_release = []
        stub_so_dir_list_debug = []
        stub_so_dir_list_release = []
        stub_so_name_win = []
        for dir_name in os.listdir("../../src/repo/lib/stub"):
            if not os.path.isdir("../../src/repo/lib/stub/" + dir_name):
                continue;
            temp_name = "../../src/repo/lib/stub/" + dir_name
            for stub_sub_dir in os.listdir(temp_name):
                if not self.is_need_stub(dir_name, stub_sub_dir):
                    continue
                stub_lib_path = "${PROJECT_SOURCE_DIR}/../../src/repo/lib/stub/" + dir_name + '/' + stub_sub_dir
                stub_dir_list_debug.append(stub_lib_path + '/Debug/' + 'lib' + stub_sub_dir + '_stub.a')
                stub_dir_list_release.append(stub_lib_path + '/Release/' + 'lib' + stub_sub_dir + '_stub.a')
                abs_service_path = abs_path + '/../../src/repo/lib/stub/' + dir_name + '/' + stub_sub_dir + '/Debug/' + 'lib' + stub_sub_dir + '.a'
                if os.path.exists(abs_service_path):
                    stub_dir_list_debug.append(stub_lib_path + '/Debug/' + 'lib' + stub_sub_dir + '.a')
                    stub_dir_list_release.append(stub_lib_path + '/Release/' + 'lib' + stub_sub_dir + '.a')
                abs_so_path = abs_path + '/../../src/repo/lib/stub/' + dir_name + '/' + stub_sub_dir + '/Debug/' + 'lib' + stub_sub_dir + '.so'
                if os.path.exists(abs_so_path):
                    stub_so_dir_list_debug.append(abs_path + '/../../src/repo/lib/stub/' + dir_name + '/' + stub_sub_dir + '/Debug/' + 'lib' + stub_sub_dir + '.so')
                    stub_so_dir_list_release.append(abs_path + '/../../src/repo/lib/stub/' + dir_name + '/' + stub_sub_dir + '/Release/' + 'lib' + stub_sub_dir + '.so')
                    stub_so_name_win.append('lib' + stub_sub_dir + '.so')
        cmake.write('  SET_TARGET_PROPERTIES(' + self.name + ' PROPERTIES LINK_FLAGS_DEBUG \"')
        for dir, name in zip(service_dir_list, service_name_list):
            cmake.write('/WHOLEARCHIVE:' + dir + "Debug/lib" + name)
        for dir, name in zip(proxy_dir_list, proxy_name_list):
            cmake.write(' /WHOLEARCHIVE:' + dir + "Debug/" + name)
        for path in stub_dir_list_debug:
            cmake.write(' /WHOLEARCHIVE:' + path)
        cmake.write('\")\n')
        cmake.write('  SET_TARGET_PROPERTIES(' + self.name + ' PROPERTIES LINK_FLAGS_RELEASE \"')
        for dir, name in zip(service_dir_list, service_name_list):
            cmake.write('/WHOLEARCHIVE:' + dir + "Release/lib" + name)
        for dir, name in zip(proxy_dir_list, proxy_name_list):
            cmake.write(' /WHOLEARCHIVE:' + dir + "Release/" + name)
        for name in stub_dir_list_release:
            cmake.write(' /WHOLEARCHIVE:' + name)
        cmake.write('\")\n')
        cmake.write('  target_link_libraries(' + self.name + '\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../win-proj/x64/Debug/service-box.lib optimized ${PROJECT_SOURCE_DIR}/../../win-proj/x64/Release/service-box.lib\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../src/repo/lib/root/Debug/librpc.a optimized ${PROJECT_SOURCE_DIR}/../../src/repo/lib/root/Release/librpc.a\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../thirdparty/curl/Debug/libcurl.a optimized ${PROJECT_SOURCE_DIR}/../../thirdparty/curl/Release/libcurl.a\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../thirdparty/zookeeper/Debug/zookeeper.lib optimized ${PROJECT_SOURCE_DIR}/../../thirdparty/zookeeper/Release/zookeeper.lib\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../thirdparty/zookeeper/Debug/hashtable.lib optimized ${PROJECT_SOURCE_DIR}/../../thirdparty/zookeeper/Release/hashtable.lib\n')
        cmake.write('    debug ${PROTOBUF_PATH}/lib/libprotobufd.lib optimized ${PROTOBUF_PATH}/lib/libprotobuf.lib\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../thirdparty/hiredis/lib/Debug/hiredis.lib optimized ${PROJECT_SOURCE_DIR}/../../thirdparty/hiredis/lib/Release/hiredis.lib\n')
        cmake.write('    debug ${PROJECT_SOURCE_DIR}/../../thirdparty/hiredis/lib/Debug/Win32_Interop.lib optimized ${PROJECT_SOURCE_DIR}/../../thirdparty/hiredis/lib/Release/Win32_Interop.lib\n')
        cmake.write('    wldap32.lib\n')
        cmake.write('    ws2_32.lib\n')
        cmake.write('    psapi.lib\n')
        cmake.write('  )\n')
        cmake.write('ELSE()\n')
        cmake.write('add_executable (' + self.name + ' ${PROJECT_SOURCE_DIR}/../../src/lib/lib_main.cpp)\n')
        cmake.write('  target_link_libraries(' + self.name + '\n')
        cmake.write('    ${PROJECT_SOURCE_DIR}/../../src/lib/libservice_box.a\n')
        cmake.write('    ${PROJECT_SOURCE_DIR}/../../src/repo/lib/root/librpc.a\n')
        cmake.write('    -lprotobuf\n')
        cmake.write('    libzookeeper.a\n')
        cmake.write('    libhashtable.a\n')
        cmake.write('    -lcurl\n')
        cmake.write('    -lpthread\n')
        cmake.write('    -ldl\n')
        cmake.write('    -lhiredis\n')
        # 遍历所有服务
        service_list = []
        for dir in os.listdir("../../src/repo/lib/"):
            if (dir != 'root') and (dir != 'proxy') and (dir != 'stub'):
                if self.is_need_service(dir):
                    service_list.append('../../src/repo/lib/' + dir + '/lib' + dir + '.a')
        proxy_list = []
        for dir_name in os.listdir("../../src/repo/lib/proxy"):
            if not os.path.isdir("../../src/repo/lib/proxy/" + dir_name):
                continue
            for proxy_sub_dir in os.listdir("../../src/repo/lib/proxy/" + dir_name):
                if self.is_need_proxy(dir_name, proxy_sub_dir):
                    proxy_lib_path = "../../src/repo/lib/proxy/" + dir_name + '/' + proxy_sub_dir
                    proxy_list.append(proxy_lib_path + '/' + 'lib' + proxy_sub_dir + '_proxy.a')
        stub_list = []
        stub_so = []
        stub_so_name = []
        for dir_name in os.listdir("../../src/repo/lib/stub"):
            if not os.path.isdir("../../src/repo/lib/stub/" + dir_name):
                continue;
            temp_name = "../../src/repo/lib/stub/" + dir_name
            for stub_sub_dir in os.listdir(temp_name):
                if self.is_need_stub(dir_name, stub_sub_dir):
                    stub_lib_path = "../../src/repo/lib/stub/" + dir_name + '/' + stub_sub_dir
                    if os.path.exists(stub_lib_path + '/' + 'lib' + stub_sub_dir + '.a'):
                        stub_list.append(stub_lib_path + '/' + 'lib' + stub_sub_dir + '.a')
                    stub_list.append(stub_lib_path + '/' + 'lib' + stub_sub_dir + '_stub.a')
                    if os.path.exists(stub_lib_path + '/' + 'lib' + stub_sub_dir + '.so'):
                        stub_so.append(stub_lib_path + '/' + 'lib' + stub_sub_dir + '.so')
                        stub_so_name.append('lib' + stub_sub_dir + '.so')
        for service in service_list:
            cmake.write('    -Wl,--whole-archive ${PROJECT_SOURCE_DIR}/' + service +' -Wl,--no-whole-archive\n')
        for proxy in proxy_list:
            cmake.write('    -Wl,--whole-archive ${PROJECT_SOURCE_DIR}/' + proxy + ' -Wl,--no-whole-archive\n')
        for stub in stub_list:
            cmake.write('    -Wl,--whole-archive ${PROJECT_SOURCE_DIR}/' + stub + ' -Wl,--no-whole-archive\n')
        cmake.write('  )\n')
        cmake.write('ENDIF()\n')
        cmake.close()
        if platform.system() == "Windows":
            os.system('cmake -G  "Visual Studio 16 2019" -A x64')
            os.system('cmake --build . --config "Release"')
            os.system('cmake --build . --config "Debug"')
            service_dir = os.path.join(self.out_dir, "service")
            if not os.path.exists(service_dir):
                os.makedirs(service_dir)
            if not os.path.exists(service_dir + '/Debug'):
                os.makedirs(service_dir + '/Debug')
            if not os.path.exists(service_dir + '/Release'):
                os.makedirs(service_dir + '/Release')
            for so_debug, so_release, so_name in zip(stub_so_dir_list_debug, stub_so_dir_list_release, stub_so_name_win):
                shutil.copyfile(so_debug, service_dir + '/Debug/' + so_name)
                shutil.copyfile(so_release, service_dir + '/Release/' + so_name)                
        else:
            os.system("cmake .")
            os.system("make")
            service_dir = os.path.join(self.out_dir, "service")
            if not os.path.exists(service_dir):
                os.makedirs(service_dir)
            for so, so_name in zip(stub_so, stub_so_name):
                shutil.copyfile(so, os.path.join(service_dir, so_name))
        config_str = open("config.default.cfg").read()
        pre_load_str = ""
        n = 0
        for dir in os.listdir("../../src/repo/lib/"):
            if (dir != 'root') and (dir != 'proxy') and (dir != 'stub'):
                for line in open("../../src/repo/src/idl/" + dir + ".service.list").read().split('\n'):
                    split_name = line.split(" ")
                    if len(split_name) > 0 and len(split_name[0]) > 0:
                        if not self.is_need_bundle(split_name[1]):
                            continue
                        if n > 0:
                            pre_load_str += "," + split_name[0] + ':"' + split_name[1] + '"'
                        else:
                            pre_load_str += split_name[0] + ":" + '"' + split_name[1] + '"'
                        n += 1
            pre_load_cfg_str = config_str.replace("preload_service=<>", "preload_service=<" + pre_load_str + ">")
            service_dir = os.path.join(self.out_dir, "service")
            config_file_in_json = self.get_config_file()
            if platform.system() == "Windows":
                if config_file_in_json is not None:
                    _, file_name = os.path.split(config_file_in_json)
                    shutil.copyfile(os.path.join(self.root_dir, config_file_in_json), self.out_dir + "/" + self.name + "/Debug/" + file_name)
                    shutil.copyfile(os.path.join(self.root_dir, config_file_in_json), self.out_dir + "/" + self.name + "/Release/" + file_name)
                else:
                    open(self.out_dir + "/" + self.name + "/Debug/" + self.name +".cfg", "w").write(pre_load_cfg_str.replace('service_dir="service"', 'service_dir="../../service/Debug"'))
                    open(self.out_dir + "/" + self.name + "/Release/" + self.name +".cfg", "w").write(pre_load_cfg_str.replace('service_dir="service"', 'service_dir="../../service/Release"'))
                shutil.copyfile("root.html", self.out_dir + "/" + self.name + "/Debug/root.html")
                shutil.copyfile("root.html", self.out_dir + "/" + self.name + "/Release/root.html")
            else:
                if not os.path.exists(self.out_dir + "/" + self.name):
                    os.makedirs(self.out_dir + "/" + self.name)
                if config_file_in_json is not None:
                    _, file_name = os.path.split(config_file_in_json)
                    shutil.copyfile(os.path.join(self.root_dir, config_file_in_json), self.out_dir + "/" + self.name + "/" + file_name)
                else:
                    open(self.out_dir + "/" + self.name + "/" + self.name +".cfg", "w").write(pre_load_cfg_str.replace('service_dir="service"', 'service_dir="../service"'))
                shutil.copyfile("root.html", self.out_dir + "/" + self.name + "/root.html")
        if not self.no_client:
            self.do_publish_client()
        g = os.walk('../../src/repo/usr/lua')
        if platform.system() == "Windows":
            for path,dir_list,file_list in g:
                for dir_name in dir_list:
                    dir_path = path + '/'+ dir_name
                    if dir_path.endswith('_lua'):
                        self.recursive_overwrite('../../src/repo/usr/lua/json', self.config["binary_name"]+'/Debug/'+os.path.basename(dir_path)+'/json')
                        self.recursive_overwrite('../../src/repo/usr/lua/proto', self.config["binary_name"]+'/Debug/'+os.path.basename(dir_path)+'/proto')
                        self.recursive_overwrite('../../src/repo/usr/lua/lua_proxy', self.config["binary_name"]+'/Debug/'+os.path.basename(dir_path)+'/lua_proxy')
                        self.recursive_overwrite('../../src/repo/usr/lua/'+os.path.basename(dir_path)+'/script', self.config["binary_name"]+'/Debug/'+os.path.basename(dir_path)+'/script')
                        self.recursive_overwrite('../../src/repo/usr/lua/'+os.path.basename(dir_path)+'/stub', self.config["binary_name"]+'/Debug/'+os.path.basename(dir_path)+'/stub')
                        self.recursive_overwrite('../../src/repo/usr/lua/json', self.config["binary_name"]+'/Release/'+os.path.basename(dir_path)+'/json')
                        self.recursive_overwrite('../../src/repo/usr/lua/proto', self.config["binary_name"]+'/Release/'+os.path.basename(dir_path)+'/proto')
                        self.recursive_overwrite('../../src/repo/usr/lua/lua_proxy', self.config["binary_name"]+'/Release/'+os.path.basename(dir_path)+'/lua_proxy')
                        self.recursive_overwrite('../../src/repo/usr/lua/'+os.path.basename(dir_path)+'/script', self.config["binary_name"]+'/Release/'+os.path.basename(dir_path)+'/script')
                        self.recursive_overwrite('../../src/repo/usr/lua/'+os.path.basename(dir_path)+'/stub', self.config["binary_name"]+'/Release/'+os.path.basename(dir_path)+'/stub')
        else:
            for path,dir_list,file_list in g:
                for dir_name in dir_list:
                    dir_path = path + '/'+ dir_name
                    if dir_path.endswith('_lua'):
                        self.recursive_overwrite('../../src/repo/usr/lua/json', self.config["binary_name"]+'/'+os.path.basename(dir_path)+'/json')
                        self.recursive_overwrite('../../src/repo/usr/lua/proto', self.config["binary_name"]+'/'+os.path.basename(dir_path)+'/proto')
                        self.recursive_overwrite('../../src/repo/usr/lua/lua_proxy', self.config["binary_name"]+'/'+os.path.basename(dir_path)+'/lua_proxy')
                        self.recursive_overwrite('../../src/repo/usr/lua/'+os.path.basename(dir_path)+'/script', self.config["binary_name"]+'/'+os.path.basename(dir_path)+'/script')
                        self.recursive_overwrite('../../src/repo/usr/lua/'+os.path.basename(dir_path)+'/stub', self.config["binary_name"]+'/'+os.path.basename(dir_path)+'/stub')

def usage():
    print("")
    print("    -h,--help                               help doc.")
    print("    -o,--out_dir                            release output root path")
    print("    -n,--name name                          service box name")
    print("    -c,--config config file path            release config file for publishing")
    print("    -d,--dir directory path                 config file root path for publishing")
    print("    --no-client                             donot publish service box client")

if __name__ == "__main__":
    if cwd != abs_path:
        os.chdir(cwd)
    opts = None
    args = None
    try:
        opts,args = getopt.getopt(sys.argv[1:],'-h-o:-n-c:-d:',['help','out_dir=','name=','config=','dir=', 'remote_repo','no-client'])
        out_dir = "."
        remote_repo = None
        name = None
        config_file = None
        no_client = False
        config_dir = None
        print_usage = False
        for opt, value in opts:
            if opt in ('-h', "--help"):
                usage()
                print_usage = True
            if opt in ("-o", "--out_dir"):
                out_dir = value
            if opt in ("--remote_repo"):
                remote_repo = value
            if opt in ('-n', '--name'):
                name = value
            if opt in ('-c', "--config"):
                config_file = value
            if opt == '--no-client':
                no_client = True
            if opt in ('-d', "--dir"):
                config_dir = value
        if not print_usage:
            if config_file is None:
                if config_dir is not None:
                    for config_file_path in os.listdir(config_dir):
                        if config_file_path.endswith(".json"):
                            CxxPublisher(out_dir, remote_repo, name, os.path.join(config_dir, config_file_path), no_client).do_publish()
            else:
                CxxPublisher(out_dir, remote_repo, name, config_file, no_client).do_publish()
    except getopt.GetoptError, e:
        print(str(e))
    
#pragma once

#include <string>
#include <sstream>

namespace kratos {
namespace service {

class ScriptDebugger;
class ScriptService;

/**
 * @brief 日志历史
 */
class LogHistory {
public:
  /**
   * 析构
   */
  virtual ~LogHistory() {}
  /**
   * @brief 设置日志等级
   * @param level 日志等级
   * @return
   */
  virtual auto set_level(int level) -> void = 0;
  /**
   * @brief 获取日志历史
   * @param limit 行数限制
   * @param [OUT] ss 输出
   * @return
   */
  virtual auto get_history(int limit, std::stringstream &ss) -> void = 0;
  /**
   * @brief 记录日志
   * @param line 日志行
   * @return
   */
  virtual auto log(const char *line) -> void = 0;
  /**
   * @brief 设置行数限制
   * @param limit 行数限制
   * @return
   */
  virtual auto set_limit(int limit) -> void = 0;
};

/**
 * @brief 脚本调试服务器，用于与外部通过websocket调试互通
 */
class ScriptDebugServer {
public:
  /**
   * @brief 析构
   */
  virtual ~ScriptDebugServer() {}
  /**
   * @brief 启动
   * @param ip IP
   * @param port 端口
   * @return true或false
   */
  virtual auto start(const std::string &ip, int port) -> bool = 0;
  /**
   * @brief 关闭
   * @return true或false
   */
  virtual auto stop() -> bool = 0;
  /**
   * @brief 主循环
   * @return
   */
  virtual auto update() -> void = 0;
  /**
   * @brief 启用调试器
   * @param name 名称
   * @param service 服务
   * @param debugger 调试器
   * @return
   */
  virtual auto enable_machine(const std::string &name, ScriptService *service,
                              ScriptDebugger *debugger) -> void = 0;
  /**
   * @brief 禁用调试器
   * @param name 名称
   * @return
   */
  virtual auto disable_machine(const std::string &name) -> void = 0;
};
} // namespace service
} // namespace kratos

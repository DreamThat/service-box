#pragma once

namespace kratos {
namespace service {

/**
 * 操作系统日志.
 */
class SysLogger {
public:
  virtual ~SysLogger() {}
  /**
   * 写入日志.
   * 
   * \param level 日志等级
   * \param fmt 日志格式
   * \param 日志参数
   * \return
   */
  virtual auto log(int level, const char *fmt, ...) -> void = 0;
};

} // namespace service
} // namespace kratos

// Machine generated code

#include "example.service.ServiceDynamic.impl.h"
#include "scheduler.hh"
#include "../console/box_console.hh"

ServiceDynamicImpl::ServiceDynamicImpl() {}

ServiceDynamicImpl::~ServiceDynamicImpl() {}

bool ServiceDynamicImpl::onAfterFork(rpc::Rpc *rpc) {
  // 获取日志接口
  logger = getContext()->get_module_logger("ServiceDynamicImpl");
  // 获取定时器管理器接口
  scheduler = getContext()->new_scheduler();
  // 注册一个的服务到目录"/service/ServiceDynamic"
  getContext()->register_service("/service/ServiceDynamic");
  // 启动一个定时器，每隔5秒执行一次并打印调用的简单统计
  scheduler->schedule_many(
      5000,
      [&](std::uint64_t, std::time_t) -> bool {
        logger->info() << "Call from peer:" << forward_call_count
                       << ", Call from box:" << backward_call_count
                       << kratos::service::EndLog();
        return true;
      }, 0
  );
  // 获取Web控制台接口
  console = getContext()->new_box_console("ServiceDynamicImpl");
  // 添加一个自定义开关
  console->add_switch("test",
  {
      [&](kratos::console::Console& console, kratos::console::ConsoleSwitch& cs)->void {
         cs.set_display_name("sample");
         cs.set_on_off(true);
         cs.set_tips("i'm a tips");
      },
      [&](kratos::console::Console& console, bool on_off, std::string& result)->bool {
         return true;
      }
  });
  // 添加一个自定义选项
  console->add_selection("test_selection",
  {
      [&](kratos::console::Console& console, kratos::console::ConsoleSelection& cs)->void {
         cs.set_display_name("sample_selection");
         cs.add_selection({"a", "a", "b", "c"});
         cs.set_tips("i'm a selection");
      },
      [&](kratos::console::Console& console, const std::string& name, std::string& result)->bool {
         return true;
      }
  });
  // 输出一条日志
  logger->info_log("ServiceDynamicImpl Initialized");
  return true;
}

bool ServiceDynamicImpl::onBeforeDestory(rpc::Rpc *rpc) {
  logger->info_log("ServiceDynamicImpl Deinitialized");
  console = nullptr;
  return true;
}

void ServiceDynamicImpl::onTick(std::time_t ms) {
  // 每帧运行一次定时器管理器主循环
  scheduler->do_schedule(ms, 1);
}

void ServiceDynamicImpl::method1(rpc::StubCallPtr call, const Data &,
                                 const std::string &) {}

std::shared_ptr<std::string>
ServiceDynamicImpl::method2(rpc::StubCallPtr call, std::int8_t,
                            const std::unordered_set<std::string> &,
                            std::uint64_t) {
  if (!login_prx) {
    login_prx = getContext()->get_proxy_from_peer<LoginProxy>(call);
  }
  if (login_prx) {
    login_prx->login("user", "pass", [&](std::uint64_t, rpc::RpcError error) {
      if (error == rpc::RpcError::NOT_FOUND) {
        logger->warn_log("target method not found");
      } else {
        backward_call_count += 1;
      }
    });
  }
  forward_call_count += 1;
  return rpc::make_return<std::string>(call, "hello world");
}

std::shared_ptr<std::unordered_map<std::int64_t, Dummy>>
ServiceDynamicImpl::method3(
    rpc::StubCallPtr call, std::int8_t, const std::unordered_set<std::string> &,
    const std::unordered_map<std::int64_t, std::string> &, std::uint64_t) {
  Dummy dummy;
  auto ptr = rpc::make_return<std::unordered_map<std::int64_t, Dummy>>(call);
  ptr->emplace(1, dummy);
  return ptr;
}

void ServiceDynamicImpl::method4(rpc::StubCallPtr call) {}

void ServiceDynamicImpl::method5(rpc::StubCallPtr call) {}

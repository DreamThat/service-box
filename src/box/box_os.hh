#pragma once

#include <string>

namespace kratos {
namespace service {
class ServiceBox;
}
} // namespace kratos

/**
 * 以守护进行方式启动.
 * \name 容器名字
 * \retval true 成功
 * \retval false 失败
 */
extern auto daemonize(const std::string &name) -> bool;

/**
 * 守护进程相关清理.
 * \name 容器名字
 */
extern auto daemon_finalize(const std::string &name) -> void;

/**
 * 安装和注册操作系统相关.
 * \param name 服务容器名字
 * \param box 服务容器
 * \retval true 成功
 * \retval false 失败
 */
extern auto install_os_things(const std::string &name,
  kratos::service::ServiceBox *box) -> bool;
/**
 * 卸载操作系统相关.
 * \param name 服务容器名字
 */
extern auto uninstall_os_things(const std::string &name) -> void;

/**
 * 获取PID文件路径.
 * \param box 服务容器
 * \return PID文件路径
 */
extern auto get_pid_file_path(kratos::service::ServiceBox *box)
    -> std::string;

/**
 * 关闭所有服务容器.
 */
extern auto close_all_box() -> void;

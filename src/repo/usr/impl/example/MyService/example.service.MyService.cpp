// Machine generated code

#include <utility>
#include "rpc_singleton.h"
#include "object_pool.h"
#include "example.service.MyService.impl.h"

MyServiceImpl::MyServiceImpl() {}
MyServiceImpl::~MyServiceImpl() {}
bool MyServiceImpl::onAfterFork(rpc::Rpc* rpc) { return true; }
bool MyServiceImpl::onBeforeDestory(rpc::Rpc* rpc) { return true; }
void MyServiceImpl::onTick(std::time_t ms) { return; }
std::int8_t MyServiceImpl::method1(rpc::StubCallPtr call) {
    return 1;
}

std::int16_t MyServiceImpl::method2(rpc::StubCallPtr call) {
    return 2;
}

std::int32_t MyServiceImpl::method3(rpc::StubCallPtr call) {
    return 3;
}

std::int64_t MyServiceImpl::method4(rpc::StubCallPtr call) {
    return 4;
}

std::uint8_t MyServiceImpl::method5(rpc::StubCallPtr call) {
    return 5;
}

std::uint16_t MyServiceImpl::method6(rpc::StubCallPtr call) {
    return 6;
}

std::uint32_t MyServiceImpl::method7(rpc::StubCallPtr call) {
    return 7;
}

std::uint64_t MyServiceImpl::method8(rpc::StubCallPtr call) {
    return 8;
}

bool MyServiceImpl::method9(rpc::StubCallPtr call) {
    return true;
}

std::shared_ptr<std::string> MyServiceImpl::method10(rpc::StubCallPtr call) {
    return rpc::make_shared_ptr<std::string>("method10");
}

float MyServiceImpl::method11(rpc::StubCallPtr call) {
    return 1.0f;
}

double MyServiceImpl::method12(rpc::StubCallPtr call) {
    return 2.0f;
}

std::shared_ptr<Data> MyServiceImpl::method13(rpc::StubCallPtr call) {
    auto data = rpc::make_shared_ptr<Data>();
    data->field1 = 1;
    data->field2 = "2";
    data->field3 = { "3", "4", "5" };
    data->field4 = { "3", "4", "5" };
    data->field5 = { {1, "1"}, {2, "2"} };
    data->field6 = true;
    data->field7 = 1.0f;
    data->field8 = 2.0f;
    data->field9.field1 = 1;
    data->field9.field2 = "2";
    data->field9.field3 = { "3", "4", "5" };
    data->field9.field4 = { "3", "4", "5" };
    data->field9.field5 = { {1, "1"}, {2, "2"} };
    data->field9.field6 = true;
    data->field9.field7 = 1.0f;
    data->field9.field8 = 2.0f;
    Dummy dummy;
    dummy.field1 = 1;
    dummy.field2 = "2";
    dummy.field3 = { "3", "4", "5" };
    dummy.field4 = { "3", "4", "5" };
    dummy.field5 = { {1, "1"}, {2, "2"} };
    dummy.field6 = true;
    dummy.field7 = 1.0f;
    dummy.field8 = 2.0f;
    data->field10.emplace_back(dummy);
    data->field10.emplace_back(dummy);
    data->field11 = { "3", "4", "5" };
    data->field12.emplace(1, dummy);
    data->field12.emplace(2, dummy);
    return data;
}

std::shared_ptr<Dummy> MyServiceImpl::method14(rpc::StubCallPtr call) {
    Dummy dummy;
    dummy.field1 = 1;
    dummy.field2 = "2";
    dummy.field3 = { "3", "4", "5" };
    dummy.field4 = { "3", "4", "5" };
    dummy.field5 = { {1, "1"}, {2, "2"} };
    dummy.field6 = true;
    dummy.field7 = 1.0f;
    dummy.field8 = 2.0f;
    return rpc::make_shared_ptr<Dummy>(dummy);
}

std::shared_ptr<std::vector<Data>> MyServiceImpl::method15(rpc::StubCallPtr call) {
    Data data;
    data.field1 = 1;
    data.field2 = "2";
    data.field3 = { "3", "4", "5" };
    data.field4 = { "3", "4", "5" };
    data.field5 = { {1, "1"}, {2, "2"} };
    data.field6 = true;
    data.field7 = 1.0f;
    data.field8 = 2.0f;
    data.field9.field1 = 1;
    data.field9.field2 = "2";
    data.field9.field3 = { "3", "4", "5" };
    data.field9.field4 = { "3", "4", "5" };
    data.field9.field5 = { {1, "1"}, {2, "2"} };
    data.field9.field6 = true;
    data.field9.field7 = 1.0f;
    data.field9.field8 = 2.0f;
    Dummy dummy;
    dummy.field1 = 1;
    dummy.field2 = "2";
    dummy.field3 = { "3", "4", "5" };
    dummy.field4 = { "3", "4", "5" };
    dummy.field5 = { {1, "1"}, {2, "2"} };
    dummy.field6 = true;
    dummy.field7 = 1.0f;
    dummy.field8 = 2.0f;
    data.field10.emplace_back(dummy);
    data.field10.emplace_back(dummy);
    data.field11 = { "3", "4", "5" };
    data.field12.emplace(1, dummy);
    data.field12.emplace(2, dummy);
    auto ret = rpc::make_shared_ptr<std::vector<Data>>();
    ret->emplace_back(data);
    ret->emplace_back(data);
    return ret;
}

std::shared_ptr<std::unordered_map<std::uint32_t,Data>> MyServiceImpl::method16(rpc::StubCallPtr call) {
    Data data;
    data.field1 = 1;
    data.field2 = "2";
    data.field3 = { "3", "4", "5" };
    data.field4 = { "3", "4", "5" };
    data.field5 = { {1, "1"}, {2, "2"} };
    data.field6 = true;
    data.field7 = 1.0f;
    data.field8 = 2.0f;
    data.field9.field1 = 1;
    data.field9.field2 = "2";
    data.field9.field3 = { "3", "4", "5" };
    data.field9.field4 = { "3", "4", "5" };
    data.field9.field5 = { {1, "1"}, {2, "2"} };
    data.field9.field6 = true;
    data.field9.field7 = 1.0f;
    data.field9.field8 = 2.0f;
    Dummy dummy;
    dummy.field1 = 1;
    dummy.field2 = "2";
    dummy.field3 = { "3", "4", "5" };
    dummy.field4 = { "3", "4", "5" };
    dummy.field5 = { {1, "1"}, {2, "2"} };
    dummy.field6 = true;
    dummy.field7 = 1.0f;
    dummy.field8 = 2.0f;
    data.field10.emplace_back(dummy);
    data.field10.emplace_back(dummy);
    data.field11 = { "3", "4", "5" };
    data.field12.emplace(1, dummy);
    data.field12.emplace(2, dummy);
    auto ret = rpc::make_shared_ptr<std::unordered_map<std::uint32_t,Data>>();
    ret->emplace(1, data);
    ret->emplace(2, data);
    return ret;
}

std::shared_ptr<std::unordered_set<std::string>> MyServiceImpl::method17(rpc::StubCallPtr call) {
    auto ret = rpc::make_shared_ptr<std::unordered_set<std::string>>();
    ret->insert("1");
    ret->insert("2");
    return ret;
}

std::int32_t MyServiceImpl::method18(rpc::StubCallPtr call, std::int8_t, std::int16_t, std::int32_t, std::int64_t, std::uint8_t, std::uint16_t, std::uint32_t, std::uint64_t, bool, const std::string&, float, double, const Data&, const std::vector<Data>&, const std::vector<std::string>&, const std::unordered_map<std::uint32_t,Data>&, const std::unordered_set<std::string>&) {
    return 1;
}


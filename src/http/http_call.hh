#pragma once

#include <cstdint>
#include <string>
#include <unordered_map>
#include <memory>

struct http_parser_settings;
struct http_parser;

namespace kratos {
namespace http {

class HttpCall;

using HeaderMap = std::unordered_map<std::string, std::string>;
using HttpCallPtr = std::weak_ptr<HttpCall>;

/**
 * HTTP调用接口
 */
class HttpCall {
public:
  virtual ~HttpCall() {}
  /**
   * 设置主机地址或域名.
   * 
   * \param host 主机地址或域名.
   * \return 
   */
  virtual auto set_host(const std::string &host) -> void = 0;
  /**
   * 获取主机地址或域名.
   * 
   * \return 主机地址或域名.
   */
  virtual auto get_host() -> const std::string & = 0;
  /**
   * 设置端口号.
   * 
   * \param port 端口号
   * \return 
   */
  virtual auto set_port(int port) -> void = 0;
  /**
   * 取得端口号.
   * 
   * \return 端口号.
   */
  virtual auto get_port() -> int = 0;
  /**
   * 获取协议头.
   * 
   * \return 协议头.
   */
  virtual auto get_headers() -> const HeaderMap & = 0;
  /**
   * @brief 设置header
   * @param headers header
   * @return 
  */
  virtual auto set_headers(const HeaderMap& headers)->void = 0;
  /**
   * 添加协议头属性.
   * 
   * \param key 键
   * \param value 值
   * \return 
   */
  virtual auto add_header(const std::string &key, const std::string &value)
      -> void = 0;
  /**
   * 获取协议体.
   * 
   * \return 协议体.
   */
  virtual auto get_content() -> std::string = 0;
  /**
   * 设置协议体.
   * 
   * \param content 协议体.
   * \return 
   */
  virtual auto set_content(const std::string &content) -> void = 0;
  /**
   * \brief 获取所有数据流，包含协议及数据
   */
  virtual auto get_full_content() -> std::string = 0;
  /**
   * 获取协议方法.
   * 
   * \return 协议方法.
   */
  virtual auto get_method() -> const std::string & = 0;
  /**
   * 设置协议方法.
   * 
   * \param method 协议方法.
   * \return 
   */
  virtual auto set_method(const std::string &method) -> void = 0;
  /**
   * 设置状态码.
   * 
   * \param code 状态码.
   * \return 
   */
  virtual auto set_status_code(int code) -> void = 0;
  /**
   * 获取状态码.
   * 
   * \return 状态码.
   */
  virtual auto get_status_code() -> int = 0;
  /**
   * 获取URI.
   * 
   * \return URI.
   */
  virtual auto get_uri() -> std::string = 0;
  /**
   * 设置URI.
   * 
   * \param url URI.
   * \return 
   */
  virtual auto set_uri(const std::string &url) -> void = 0;
  /**
   * \brief 完成HTTP响应并发送给客户端,用于异步完成的响应
   */
  virtual auto finish() -> void = 0;
  /**
   * \brief 获取用户响应，填写数据
   */
  virtual auto get_response() -> HttpCallPtr = 0;
};

} // namespace http
} // namespace kratos

#pragma once

#include <ctime>

namespace kratos {
namespace util {

/**
 * 获取当前的系统时间戳，毫秒.
 */
extern auto get_os_time_millionsecond() -> std::time_t;

/**
 * 获取框架上次主循环记录的最新时间戳，毫秒.
 */
extern auto get_last_timestamp_millionsecond() -> std::time_t;

/**
 * 设置框架上次主循环记录的最新时间戳，毫秒. 在每一帧的最开始调用
 */
extern auto set_last_timestamp_millionsecond(std::time_t now) -> void;

/**
 * 获取最低的帧消耗，毫秒.
 */
extern auto get_lowest_cost_millionsecond() -> std::time_t;

/**
 * 获取最高的帧消耗，毫秒.
 */
extern auto get_highest_cost_millionsecond() -> std::time_t;

/**
 * 设置帧开始时间戳，毫秒.
 */
extern auto set_frame_begin(std::time_t now) -> void;

/**
 * 设置帧结束时间戳，毫秒.
 */
extern auto set_frame_end(std::time_t now) -> void;

/**
 * 获取现在实际帧率.
 */
extern auto get_real_frame() -> int;

/**
 * 获取当前帧的消耗，毫秒.
 */
extern auto get_current_cost_millionsecond()->std::time_t;

/**
 * 获取操作系统当前微秒.
 */
extern auto get_os_time_microsecond() -> std::time_t;

} // namespace util
} // namespace kratos

#pragma once

#include <ctime>
#include <memory>
#include <string>

#include "../../box/script_service.hh"


namespace kratos {
namespace lua {

/**
 * Lua脚本服务
 *
 * 负责加载脚本并接入RPC框架实现
 * 1. Lua服务
 * 2. Lua内调用远程服务
 * 3. 自动热更新Lua脚本
 */
class LuaService : public kratos::service::ScriptService {
public:
  virtual ~LuaService() {}
  /**
   * 启动, 根据服务UUID读取配置
   *
   * \param uuid 服务UUID
   * \return true或false
   */
  virtual auto start(rpc::ServiceUUID uuid) -> bool = 0;
  /**
   * 停止
   */
  virtual auto stop() -> bool = 0;
  /**
   * 主循环
   */
  virtual auto update(std::time_t tick) -> void = 0;
  /**
   * 调用服务方法
   */
  virtual auto call(rpc::StubCallPtr stub_call) -> void = 0;
  /**
   * 热更新，以文件为单元
   *
   * \param file_path 需要加载的文件
   * \return true或false
   */
  virtual auto hotfix_file(const std::string &file_path) -> bool = 0;
  /**
   * 热更新, 热更新一个代码字符串
   *
   * \param chunk 一段代码
   * \return true或false
   */
  virtual auto hotfix_chunk(const std::string &chunk) -> bool = 0;
  /**
   * 上一次服务调用是否yield
   */
  virtual auto is_last_call_yield() -> bool = 0;
  /**
   * @brief 开启调试器
   * @param name 调试器名称
   * @return
  */
  virtual auto open_debugger(const std::string& name)->void = 0;
  /**
   * @brief 关闭调试器
   * @return 
  */
  virtual auto close_debugger()->void = 0;
  /**
   * @brief 启用调试器
   * @return 
  */
  virtual auto disable_debugger()->void = 0;
  /**
   * @brief 禁用调试器
   * @return 
  */
  virtual auto enable_debugger()->void= 0;
  /**
   * @brief 重新加载所有脚本
   * @return true或false
  */
  virtual auto reload() -> bool = 0;
  virtual auto restart() -> bool = 0;
  /**
   * @brief 获取协程信息
   * @return 协程信息
  */
  virtual auto get_thread_info() -> std::string = 0;
  virtual auto get_log_history() -> kratos::service::LogHistory* = 0;
};

} // namespace lua
} // namespace kratos

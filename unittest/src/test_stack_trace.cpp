#include "../../src/detail/stack_trace_linux.hh"
#include "../../src/detail/stack_trace_windows.hh"
#include "../framework/unittest.hh"
#include <iostream>
#include <stdlib.h>

FIXTURE_BEGIN(test_stack_trace)

// NOTICE
// 下面的崩溃测试的CASE不要在调试器内运行，调试器会接管异常处理

CASE(TestStackTrace1) {
  auto trace = kratos::util::StackTrace::get_stack_frame_info();
  std::cout << trace << std::endl;
}

void crash_func1(int i) { auto j = 10 / i; }

CASE(TestStackTrace2) {
  kratos::util::StackTrace::install_system_exception(nullptr);
  ASSERT_EXCEPT(crash_func1(0));
  kratos::util::StackTrace::uninstall_system_exception();
}

void crash_func2(int *i) { auto j = *i; }

CASE(TestStackTrace3) {
  kratos::util::StackTrace::install_system_exception(nullptr);
  ASSERT_EXCEPT(crash_func2(nullptr));
  kratos::util::StackTrace::uninstall_system_exception();
}

void crash_func3() { abort(); }

CASE(TestStackTrace4) {
#ifdef WIN32
  kratos::util::StackTrace::install_system_exception(nullptr);
  ASSERT_EXCEPT(crash_func3());
  kratos::util::StackTrace::uninstall_system_exception();
#endif // WIN32
}

FIXTURE_END(test_argument)

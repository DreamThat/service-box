#pragma once

#ifndef WIN32

#include <string>

namespace kratos {
namespace service {
class ServiceBox;
} // namespace service
} // namespace kratos

namespace kratos {
namespace util{

constexpr static int MAX_FRAMES = 64; ///< 最大堆栈深度

class StackTrace {
  StackTrace() = delete;
  StackTrace(const StackTrace &) = delete;
  StackTrace(StackTrace &&) = delete;
  const StackTrace &operator=(const StackTrace &) = delete;
  const StackTrace &operator=(StackTrace &&) = delete;

public:
  /**
   * 获取当前调用堆栈，打印深度最大为max_frames.
   *
   * \param max_frames 打印深度最大值，默认为MAX_FRAMES
   * \return 堆栈信息
   */
  auto static get_stack_frame_info(int max_frames = MAX_FRAMES) -> std::string;

  /**
   * 抛出一个SystemException.
   * 
   * \param signal 异常信号
   * \return 
   */
  auto static throw_system_exception(int signal) -> void;

  /**
   * 安装系统异常处理.
   *
   * \return true或false
   */
  auto static install_system_exception(kratos::service::ServiceBox *box)
      -> bool;
  /**
   * 卸载系统异常处理.
   *
   * \return
   */
  auto static uninstall_system_exception() -> void;
};

}
}

#endif // WIN32

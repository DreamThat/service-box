#pragma once

#include <cstdlib>
#include <unordered_map>

namespace kratos {
namespace service {
/**
 * 分配内存
 * @param size 需要分配的字节数量
 * @return 内存地址
 */
extern char *box_malloc(std::size_t size);
/**
 * 释放内存
 * @param p 内存地址
 */
extern void box_free(void *p);

} // namespace service
} // namespace kratos

#pragma once

#include "../../service_register/service_register.hh"
#include "../../util/box_std_allocator.hh"
#include "../../util/object_pool.hh"
#include <memory>
#include <string>
#include <unordered_map>

namespace kratos {
namespace service {

class ZookeeperClient;

class ServiceRegisterZookeeper : public ServiceRegister {
  using AlreadyRegisterMap =
      std::unordered_map<
        std::string,
        std::string,
        std::hash<std::string>,
        std::equal_to<std::string>,
        kratos::service::Allocator<std::pair<const std::string, std::string>>
      >;
  unique_pool_ptr<ZookeeperClient> zoo_ptr_{nullptr};     ///< Zookeeper client实例
  std::time_t                      last_tick_{0};         ///< 上一次update的时间戳，毫秒
  constexpr static std::time_t     CHECK_INTERVAL = 5000; ///< 断线检测周期，毫秒
  AlreadyRegisterMap               already_register_map_; ///< 已经注册的服务

public:
  ServiceRegisterZookeeper();
  virtual ~ServiceRegisterZookeeper();
  virtual bool start(const std::string &servers, int timeout) override;
  virtual bool stop() override;
  virtual void update(std::time_t tick) override;
  virtual bool register_service(const std::string &name,
                                const std::string &host) override;
  virtual bool unregister_service(const std::string &name,
                                  const std::string &host) override;
};

} // namespace service
} // namespace kratos

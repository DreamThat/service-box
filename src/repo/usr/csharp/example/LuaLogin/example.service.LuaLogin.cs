// Machine generated code

using System.Collections.Generic;
using kratos;

namespace rpc {

//interface->
internal abstract class LuaLogin : IService {
    uint id_ = 0;
    public ulong UUID { get { return 5871407833815435533; } }
    public uint ID { get { return id_; } set { id_ = value; } }
    public virtual void OnTick(long ms) {}
    public abstract ulong login(string arg1,string arg2);
    public abstract void logout(ulong arg1);
    public abstract void oneway_method();
    public abstract void normal_method();
}

//interface<-
}


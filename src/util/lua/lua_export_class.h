#pragma once

#include "../../../thirdparty/lua/src/lua.hpp"
#include "../object_pool.hh"
#include "lua_util.hh"
#include <string>
#include <ctime>

namespace kratos {
namespace lua {

/**
 * @brief Lua导出模块包装
 */
class LuaExportClass {
public:
  /**
   * @brief 析构
   */
  virtual ~LuaExportClass() {}
  /**
   * @brief 注册lua函数
   * @return true或false
   */
  virtual auto do_register() -> bool = 0;
  /**
   * @brief 主循环
   * @param ms 当前时间戳，毫秒
   * @return
   */
  virtual auto update(std::time_t ms) -> void = 0;
  /**
   * @brief 清理
   * @return 
  */
  virtual auto do_cleanup() -> void = 0;

protected:
  /**
   * @brief 获取指定名字的全局表指针
   * @tparam T 类型
   * @param l 虚拟机
   * @param name 名字
   * @return 指针
   */
  template <typename T> static T *get_class(lua_State *l, const char *name) {
    LuaUtil::get_registry_value(l, name);
    return LuaUtil::pop_ptr_value<T *>(l);
  }
  /**
   * @brief 设置指定名字的全局表指针
   * @tparam T 类型
   * @param l 虚拟机
   * @param t 指针
   * @param name 名字
   * @return
   */
  template <typename T>
  static auto set_class(lua_State *l, T *t, const char *name) -> void {
    LuaUtil::lua_push(l, (void *)t);
    lua_setfield(l, LUA_REGISTRYINDEX, name);
  }
};

} // namespace lua
} // namespace kratos

#pragma once

#include "../../../thirdparty/klogger/klogger/interface/logger.h"
#include "../box_std_allocator.hh"
#include "../../detail/script_debug_server.hh"
#include <deque>
#include <sstream>
#include <string>

namespace kratos {
namespace lua {

/**
 * @brief 日志历史
 */
class LogHistoryImpl : public kratos::service::LogHistory {
  using LogVector = std::deque<std::string>;
  int level_{klogger::Logger::VERBOSE}; ///< 日志等级限制
  LogVector log_history_;               ///< 日志历史
  std::size_t max_line_{50};            ///< 最大历史行数

public:
  /**
   * @brief 构造
   */
  LogHistoryImpl();
  /**
   * 析构
   */
  virtual ~LogHistoryImpl();
  /**
   * @brief 设置日志等级
   * @param level 日志等级
   * @return
   */
  virtual auto set_level(int level) -> void override;
  /**
   * @brief 获取日志历史
   * @param limit 行数限制
   * @param [OUT] ss 输出
   * @return
   */
  virtual auto get_history(int limit, std::stringstream &ss) -> void override;
  /**
   * @brief 记录日志
   * @param line 日志行
   * @return
   */
  virtual auto log(const char *line) -> void override;
  /**
   * @brief 设置行数限制
   * @param limit 行数限制
   * @return
   */
  virtual auto set_limit(int limit) -> void override;
};

} // namespace lua
} // namespace kratos

#pragma once

#include "../lua_export_class.h"

namespace kratos {
namespace service {
class ServiceBox;
}
} // namespace kratos

namespace kratos {
namespace lua {

class LuaServiceImpl;

/**
 * @brief 参数类lua导出包装
 */
class LuaArgument : public LuaExportClass {
  kratos::service::ServiceBox *box_{nullptr}; ///< 服务容器
  lua_State *L_{nullptr};                     ///< lua虚拟机
  LuaServiceImpl *service_{nullptr};          ///< Lua服务

public:
  /**
   * @brief 构造
   * @param box 服务容器
   * @param L 虚拟机
   */
  LuaArgument(kratos::service::ServiceBox *box, lua_State *L,
              LuaServiceImpl *service);
  /**
   * 析构
   */
  virtual ~LuaArgument();
  virtual auto do_register() -> bool override;
  virtual auto update(std::time_t ms) -> void override;
  virtual auto do_cleanup() -> void override;

private:
  static int lua_get_config_file_path(lua_State *l);
  static int lua_get_config_file_name(lua_State *l);
  static int lua_get_max_frame(lua_State *l);
  static int lua_is_daemon(lua_State *l);
  static int lua_get_config_center_api_url(lua_State *l);
  static int lua_is_open_system_exception(lua_State *l);
};

} // namespace lua
} // namespace kratos

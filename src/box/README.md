# 目录内容概述

目录内包含了service box的核心功能类。

# 核心功能类

1. [kratos::service::BoxChannel](https://gitee.com/dennis-kk/service-box/blob/master/src/box/README-box_channel.md)
2. [kratos::service::BoxNetwork](https://gitee.com/dennis-kk/service-box/blob/master/src/box/README-box_network.md)
3. [kratos::MemPool,kratos::FixedMemPool](https://gitee.com/dennis-kk/service-box/blob/master/src/box/README-fixed_mem_pool.md)
4. [kratos::service::LocalCommand](https://gitee.com/dennis-kk/service-box/blob/master/src/box/README-local_command.md)
5. [kratos::service::ServiceHttpLoader](https://gitee.com/dennis-kk/service-box/blob/master/src/box/README-service_http_loader.md)
6. [kratos::service::MemoryAllocator](https://gitee.com/dennis-kk/service-box/blob/master/src/box/README-memory_allocator.md)
7. [kratos::service::ServiceBox](https://gitee.com/dennis-kk/service-box/blob/master/src/box/README-service_box.md)
8. [kratos::service::ServiceContext](https://gitee.com/dennis-kk/service-box/blob/master/src/box/README-service_context.md)
#pragma once

#include <string>
#include "console.hh"

namespace kratos {
namespace console {

class BoxConsole {
public:
  virtual ~BoxConsole() {}
  /**
   * 添加控制台开关plugin
   *
   * \param name 开关名字
   * \param switch_plugin plugin
   */
  virtual auto add_switch(const std::string& name, ConsoleSwitchPlugin switch_plugin)->void = 0;
  /**
   * 移除控制台开关plugin
   *
   * \param name 开关名字
   */
  virtual auto remove_switch(const std::string& name)->void = 0;
  /**
   * 添加控制台选项plugin
   *
   * \param name 选项名字
   * \param selection_plugin plugin
   */
  virtual auto add_selection(const std::string& name, ConsoleSelectionPlugin selection_plugin)->void = 0;
  /**
   * 移除控制台选项plugin
   *
   * \param name 选项名字
   */
  virtual auto remove_selection(const std::string& name)->void = 0;
};
}
}

#pragma once

#include <string>
#include <list>
#include <functional>

namespace k {
namespace test {

class Testcase;

using FixtureSetupMethod = std::function<void()>;
using FixtureTearDownMethod = std::function<void()>;

// 用例集合
class Fixture {
    using TestcaseList = std::list<Testcase*>;
    TestcaseList caseList_; // 用例列表
    std::string name_; // 名称
    bool run_; // 是否运行
    FixtureSetupMethod setup_; // 初始化函数
    FixtureTearDownMethod tearDown_; // 清理函数
    std::size_t passed_;
    std::size_t failed_;

public:
    // 构造
    // @param name 名称
    Fixture(const std::string& name);
    // 析构
    virtual ~Fixture();
    // 初始化
    void setup();
    // 清理
    void tearDown();
    // 设置初始化函数
    bool onSetup(FixtureSetupMethod method);
    // 设置清理函数
    bool onTearDown(FixtureTearDownMethod method);
    // 取得名称
    const std::string& getName() const;
    // 添加测试用例
    bool addTestcase(Testcase* testcase);
    // 取得测试用例
    // @param name 用例名称
    Testcase* getTestcase(const std::string& name);
    // 禁用用例
    // @param name 用例名称
    void disableTestcase(const std::string& name);
    // 禁用所用用例
    void disableAllTestCase();
    // 开启用例
    // @param name 用例名称
    void enableTestcase(const std::string& name);
    // 运行所有用例
    void runAllTestcase();
    // 是否允许运行
    bool isEnable();
    // 禁止运行
    void disable();
    // 允许运行
    void enable();
    // Returns the count of passed cases
    std::size_t getPassCases();
    // Returns the count of failed cases
    std::size_t getFailedCases();
    // 获取所有用例
    const TestcaseList& getAllCase();
};

}
}

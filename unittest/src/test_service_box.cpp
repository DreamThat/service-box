#include "../../src//repo/src/include/example/ServiceDynamic/proxy/example.service.ServiceDynamic.proxy.h"
#include "../../src/argument/box_argument.hh"
#include "../../src/box/service_box.hh"
#include "../../src/box/service_http_loader.hh"
#include "../../src/detail/service_context_impl.hh"
#include "../../src/repo/src/include/root/rpc_root.h"
#include "../../src/util/os_util.hh"
#include "../../src/util/time_util.hh"
#include "../framework/unittest.hh"
#include <fstream>

FIXTURE_BEGIN(test_service_box)

#ifdef WIN32
#ifdef _DEBUG
SETUP([]() {
  kratos::util::make_dir("service");
  std::ofstream ofs_default;
  ofs_default.open("box.cfg", std::ios::trunc | std::ios::out);
  ofs_default << "listener = {"
      "   host = (\"127.0.0.1:10001\")"
      "}"
      "box_channel_recv_buffer_len = 1024 "
      "box_name = \"test_box\""
      "connect_other_box_timeout = 2000 "
      "service_finder_connect_timeout = 2000 "
      "logger_config_line = "
      "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
      "open_coroutine = \"false\"";
  ofs_default.close();

  std::ofstream ofs;
  ofs.open("config.cfg", std::ios::trunc | std::ios::out);
  ofs << "listener = {"
         "   host = (\"127.0.0.1:10001\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\"";
  ofs.close();

  std::ofstream ofs_op;
  ofs_op.open("config_op.cfg", std::ios::trunc | std::ios::out);
  ofs_op << "listener = {"
            "   host = (\"127.0.0.1:10002\")"
            "}"
            "box_channel_recv_buffer_len = 1024 "
            "box_name = \"test_box\""
            "connect_other_box_timeout = 2000 "
            "service_finder_connect_timeout = 2000 "
            "logger_config_line = "
            "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
            "open_coroutine = \"false\" "
            "service_finder = {"
            "   hosts = \"127.0.0.1:2181\""
            "}";
  ofs_op.close();

  std::ofstream ofs_op_1;
  ofs_op_1.open("config_op_1.cfg", std::ios::trunc | std::ios::out);
  ofs_op_1
      << "listener = {"
         "   host = (\"127.0.0.1:10004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic/Debug\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_1.close();

  std::ofstream ofs_op_2;
  ofs_op_2.open("config_op_2.cfg", std::ios::trunc | std::ios::out);
  ofs_op_2
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic/Debug\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_2.close();

  std::ofstream ofs_ob;
  ofs_ob.open("config_ob.cfg", std::ios::trunc | std::ios::out);
  ofs_ob << "listener = {"
            "   host = (\"127.0.0.1:10003\")"
            "}"
            "box_channel_recv_buffer_len = 1024 "
            "box_name = \"test_box\""
            "connect_other_box_timeout = 2000 "
            "service_finder_connect_timeout = 2000 "
            "logger_config_line = "
            "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
            "open_coroutine = \"false\""
            "service_finder = {"
            "   hosts = \"127.0.0.1:2181\""
            "}"
            "necessary_service = (\"op_service\")";
  ofs_ob.close();

  std::ofstream ofs_ob_1;
  ofs_ob_1.open("config_ob_1.cfg", std::ios::trunc | std::ios::out);
  ofs_ob_1 << "listener = {"
              "   host = (\"127.0.0.1:10005\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service_finder = {"
              "   hosts = \"127.0.0.1:2181\""
              "}"
              "necessary_service = (\"dynamic\")";
  ofs_ob_1.close();

  std::ofstream ofs_ob_2;
  ofs_ob_2.open("config_ob_2.cfg", std::ios::trunc | std::ios::out);
  ofs_ob_2 << "listener = {"
              "   host = (\"127.0.0.1:11005\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service_finder = {"
              "   hosts = \"127.0.0.1:2181\""
              "}"
              "necessary_service = (\"dynamic\")";
  ofs_ob_2.close();

  std::ofstream ofs_bad;
  ofs_bad.open("config_bad.cfg", std::ios::trunc | std::ios::out);
  ofs_bad << "listener = {"
             "   host = (\"127.0.0.1:10003\")"
             "}"
             "box_channel_recv_buffer_len = 1024 "
             "box_name = \"test_box\""
             "connect_other_box_timeout = 2000 "
             "service_finder_connect_timeout = 2000 "
             "logger_config_line = "
             "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
             "open_coroutine = \"false\""
             "service_finder = {"
             "   hosts = \"127.0.0.1:2181\""
             "}"
             "necessary_service = 1"; // ���ʹ��������
  ofs_bad.close();

  std::ofstream ofs_bad_1;
  ofs_bad_1.open("config_bad_1.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_1 << "listener = {"
               "   host = (\"127.0.0.1\")" // ���������
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_1.close();

  std::ofstream ofs_bad_2;
  ofs_bad_2.open("config_bad_2.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_2 << "listener = {"
               "   host = (\"127.0.0.1:xxx\")" // ���������
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_2.close();

  std::ofstream ofs_bad_3;
  ofs_bad_3.open("config_bad_3.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_3 << "listener = {"
               "   host = (\"xxxxx:12345\")" // ���������
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_3.close();

  std::ofstream ofs_bad_4;
  ofs_bad_4.open("config_bad_4.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_4 << "listener = {"
               "   host = (\"127.0.0.1:10003\")"
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = \"sjkdfjksjfksjdfk\" " // ���������
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_4.close();

  std::ofstream ofs_bad_5;
  ofs_bad_5.open("config_bad_5.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_5 << "listener = {"
               "   host = (\"127.0.0.1:10001\")"
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\" "
               "service_finder = {"
               "   type = \"unknown\""
               "}";
  ofs_bad_5.close();

  std::ofstream ofs_hl_1;
  ofs_hl_1.open("ofs_hl_1.cfg", std::ios::trunc | std::ios::out);
  ofs_hl_1 << "listener = {"
              "   host = (\"127.0.0.1:11004\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service = { "
              "   service_dir = \"service\" "
              "   remote_service_repo_version_api = "
              "\"http://127.0.0.1/doc/version.json\" "
              "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
              "   remote_service_repo_latest_version_api= "
              "\"http://127.0.0.1/doc/version.json\" "
              "   is_open_remote_update = \"true\" "
              "   remote_repo_check_interval = 1 "
              "}";
  ofs_hl_1.close();

  std::ofstream ofs_hl_wrong_version_api;
  ofs_hl_wrong_version_api.open("ofs_hl_wrong_version_api.cfg",
                                std::ios::trunc | std::ios::out);
  ofs_hl_wrong_version_api
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service = { "
         "   service_dir = \"service\" "
         "   remote_service_repo_version_api = \"http://wrong_api\" "
         "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
         "   remote_service_repo_latest_version_api= \"http://wrong_api\" "
         "   is_open_remote_update = \"true\" "
         "   remote_repo_check_interval = 1 "
         "}";
  ofs_hl_wrong_version_api.close();

  std::ofstream ofs_hl_wrong_version_file;
  ofs_hl_wrong_version_file.open("ofs_hl_wrong_version_file.cfg",
                                 std::ios::trunc | std::ios::out);
  ofs_hl_wrong_version_file
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service = { "
         "   service_dir = \"service\" "
         "   remote_service_repo_version_api = \"http://wrong_api\" "
         "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
         "   remote_service_repo_latest_version_api= "
         "\"http://127.0.0.1/doc/wrong_version.json\" "
         "   is_open_remote_update = \"true\" "
         "   remote_repo_check_interval = 1 "
         "}";
  ofs_hl_wrong_version_file.close();

  std::ofstream ofs_hl_wrong_version_bundle;
  ofs_hl_wrong_version_bundle.open("ofs_hl_wrong_version_bundle.cfg",
                                   std::ios::trunc | std::ios::out);
  ofs_hl_wrong_version_bundle
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service = { "
         "   service_dir = \"service\" "
         "   remote_service_repo_version_api = \"http://wrong_api\" "
         "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
         "   remote_service_repo_latest_version_api= "
         "\"http://127.0.0.1/doc/wrong_version_bundle.json\" "
         "   is_open_remote_update = \"true\" "
         "   remote_repo_check_interval = 1 "
         "}";
  ofs_hl_wrong_version_bundle.close();

  std::ofstream ofs_hl_2;
  ofs_hl_2.open("ofs_hl_2.cfg", std::ios::trunc | std::ios::out);
  ofs_hl_2 << "listener = {"
              "   host = (\"127.0.0.1:11004\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service = { "
              "   service_dir = \"service\" "
              "   remote_service_repo_version_api = "
              "\"http://127.0.0.1/doc/version1.json\" "
              "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
              "   is_open_remote_update = \"true\" "
              "   remote_repo_check_interval = 1 "
              "}";
  ofs_hl_2.close();
})
#else
SETUP([]() {
  kratos::util::make_dir("service");
  std::ofstream ofs_default;
  ofs_default.open("box.cfg", std::ios::trunc | std::ios::out);
  ofs_default << "listener = {"
      "   host = (\"127.0.0.1:10001\")"
      "}"
      "box_channel_recv_buffer_len = 1024 "
      "box_name = \"test_box\""
      "connect_other_box_timeout = 2000 "
      "service_finder_connect_timeout = 2000 "
      "logger_config_line = "
      "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
      "open_coroutine = \"false\"";
  ofs_default.close();

  std::ofstream ofs;
  ofs.open("config.cfg", std::ios::trunc | std::ios::out);
  ofs << "listener = {"
         "   host = (\"127.0.0.1:10001\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\"";
  ofs.close();

  std::ofstream ofs_op;
  ofs_op.open("config_op.cfg", std::ios::trunc | std::ios::out);
  ofs_op << "listener = {"
            "   host = (\"127.0.0.1:10002\")"
            "}"
            "box_channel_recv_buffer_len = 1024 "
            "box_name = \"test_box\""
            "connect_other_box_timeout = 2000 "
            "service_finder_connect_timeout = 2000 "
            "logger_config_line = "
            "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
            "open_coroutine = \"false\" "
            "service_finder = {"
            "   hosts = \"127.0.0.1:2181\""
            "}";
  ofs_op.close();

  std::ofstream ofs_op_1;
  ofs_op_1.open("config_op_1.cfg", std::ios::trunc | std::ios::out);
  ofs_op_1
      << "listener = {"
         "   host = (\"127.0.0.1:10004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic/Release\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_1.close();

  std::ofstream ofs_op_2;
  ofs_op_2.open("config_op_2.cfg", std::ios::trunc | std::ios::out);
  ofs_op_2
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic/Release\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_2.close();

  std::ofstream ofs_ob;
  ofs_ob.open("config_ob.cfg", std::ios::trunc | std::ios::out);
  ofs_ob << "listener = {"
            "   host = (\"127.0.0.1:10003\")"
            "}"
            "box_channel_recv_buffer_len = 1024 "
            "box_name = \"test_box\""
            "connect_other_box_timeout = 2000 "
            "service_finder_connect_timeout = 2000 "
            "logger_config_line = "
            "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
            "open_coroutine = \"false\""
            "service_finder = {"
            "   hosts = \"127.0.0.1:2181\""
            "}"
            "necessary_service = (\"op_service\")";
  ofs_ob.close();

  std::ofstream ofs_ob_1;
  ofs_ob_1.open("config_ob_1.cfg", std::ios::trunc | std::ios::out);
  ofs_ob_1 << "listener = {"
              "   host = (\"127.0.0.1:10005\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service_finder = {"
              "   hosts = \"127.0.0.1:2181\""
              "}"
              "necessary_service = (\"dynamic\")";
  ofs_ob_1.close();

  std::ofstream ofs_ob_2;
  ofs_ob_2.open("config_ob_2.cfg", std::ios::trunc | std::ios::out);
  ofs_ob_2 << "listener = {"
              "   host = (\"127.0.0.1:11005\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service_finder = {"
              "   hosts = \"127.0.0.1:2181\""
              "}"
              "necessary_service = (\"dynamic\")";
  ofs_ob_2.close();

  std::ofstream ofs_bad;
  ofs_bad.open("config_bad.cfg", std::ios::trunc | std::ios::out);
  ofs_bad << "listener = {"
             "   host = (\"127.0.0.1:10003\")"
             "}"
             "box_channel_recv_buffer_len = 1024 "
             "box_name = \"test_box\""
             "connect_other_box_timeout = 2000 "
             "service_finder_connect_timeout = 2000 "
             "logger_config_line = "
             "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
             "open_coroutine = \"false\""
             "service_finder = {"
             "   hosts = \"127.0.0.1:2181\""
             "}"
             "necessary_service = 1"; // ���ʹ��������
  ofs_bad.close();

  std::ofstream ofs_bad_1;
  ofs_bad_1.open("config_bad_1.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_1 << "listener = {"
               "   host = (\"127.0.0.1\")" // ���������
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_1.close();

  std::ofstream ofs_bad_2;
  ofs_bad_2.open("config_bad_2.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_2 << "listener = {"
               "   host = (\"127.0.0.1:xxx\")" // ���������
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_2.close();

  std::ofstream ofs_bad_3;
  ofs_bad_3.open("config_bad_3.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_3 << "listener = {"
               "   host = (\"xxxxx:12345\")" // ���������
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_3.close();

  std::ofstream ofs_bad_4;
  ofs_bad_4.open("config_bad_4.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_4 << "listener = {"
               "   host = (\"127.0.0.1:10003\")"
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = \"sjkdfjksjfksjdfk\" " // ���������
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_4.close();

  std::ofstream ofs_bad_5;
  ofs_bad_5.open("config_bad_5.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_5 << "listener = {"
               "   host = (\"127.0.0.1:10001\")"
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\" "
               "service_finder = {"
               "   type = \"unknown\""
               "}";
  ofs_bad_5.close();

  std::ofstream ofs_hl_1;
  ofs_hl_1.open("ofs_hl_1.cfg", std::ios::trunc | std::ios::out);
  ofs_hl_1 << "listener = {"
              "   host = (\"127.0.0.1:11004\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service = { "
              "   service_dir = \"service\" "
              "   remote_service_repo_version_api = "
              "\"http://127.0.0.1/doc/version_release.json\" "
              "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
              "   remote_service_repo_latest_version_api= "
              "\"http://127.0.0.1/doc/version_release.json\" "
              "   is_open_remote_update = \"true\" "
              "   remote_repo_check_interval = 1 "
              "}";
  ofs_hl_1.close();

  std::ofstream ofs_hl_wrong_version_api;
  ofs_hl_wrong_version_api.open("ofs_hl_wrong_version_api.cfg",
                                std::ios::trunc | std::ios::out);
  ofs_hl_wrong_version_api
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service = { "
         "   service_dir = \"service\" "
         "   remote_service_repo_version_api = \"http://wrong_api\" "
         "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
         "   remote_service_repo_latest_version_api= \"http://wrong_api\" "
         "   is_open_remote_update = \"true\" "
         "   remote_repo_check_interval = 1 "
         "}";
  ofs_hl_wrong_version_api.close();

  std::ofstream ofs_hl_wrong_version_file;
  ofs_hl_wrong_version_file.open("ofs_hl_wrong_version_file.cfg",
                                 std::ios::trunc | std::ios::out);
  ofs_hl_wrong_version_file
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service = { "
         "   service_dir = \"service\" "
         "   remote_service_repo_version_api = \"http://wrong_api\" "
         "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
         "   remote_service_repo_latest_version_api= "
         "\"http://127.0.0.1/doc/wrong_version.json\" "
         "   is_open_remote_update = \"true\" "
         "   remote_repo_check_interval = 1 "
         "}";
  ofs_hl_wrong_version_file.close();

  std::ofstream ofs_hl_wrong_version_bundle;
  ofs_hl_wrong_version_bundle.open("ofs_hl_wrong_version_bundle.cfg",
                                   std::ios::trunc | std::ios::out);
  ofs_hl_wrong_version_bundle
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service = { "
         "   service_dir = \"service\" "
         "   remote_service_repo_version_api = \"http://wrong_api\" "
         "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
         "   remote_service_repo_latest_version_api= "
         "\"http://127.0.0.1/doc/wrong_version_bundle.json\" "
         "   is_open_remote_update = \"true\" "
         "   remote_repo_check_interval = 1 "
         "}";
  ofs_hl_wrong_version_bundle.close();

  std::ofstream ofs_hl_2;
  ofs_hl_2.open("ofs_hl_2.cfg", std::ios::trunc | std::ios::out);
  ofs_hl_2 << "listener = {"
              "   host = (\"127.0.0.1:11004\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service = { "
              "   service_dir = \"service\" "
              "   remote_service_repo_version_api = "
              "\"http://127.0.0.1/doc/version1_release.json\" "
              "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
              "   is_open_remote_update = \"true\" "
              "   remote_repo_check_interval = 1 "
              "}";
  ofs_hl_2.close();
})
#endif // _DEBUG
#else
SETUP([]() {
  kratos::util::make_dir("service");
  std::ofstream ofs_default;
  ofs_default.open("box.cfg", std::ios::trunc | std::ios::out);
  ofs_default << "listener = {"
      "   host = (\"127.0.0.1:10001\")"
      "}"
      "box_channel_recv_buffer_len = 1024 "
      "box_name = \"test_box\""
      "connect_other_box_timeout = 2000 "
      "service_finder_connect_timeout = 2000 "
      "logger_config_line = "
      "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
      "open_coroutine = \"false\"";
  ofs_default.close();

  std::ofstream ofs;
  ofs.open("config.cfg", std::ios::trunc | std::ios::out);
  ofs << "listener = {"
         "   host = (\"127.0.0.1:10001\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\"";
  ofs.close();

  std::ofstream ofs_op;
  ofs_op.open("config_op.cfg", std::ios::trunc | std::ios::out);
  ofs_op << "listener = {"
            "   host = (\"127.0.0.1:10002\")"
            "}"
            "box_channel_recv_buffer_len = 1024 "
            "box_name = \"test_box\""
            "connect_other_box_timeout = 2000 "
            "service_finder_connect_timeout = 2000 "
            "logger_config_line = "
            "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
            "open_coroutine = \"false\" "
            "service_finder = {"
            "   hosts = \"127.0.0.1:2181\""
            "}";
  ofs_op.close();

  std::ofstream ofs_op_1;
  ofs_op_1.open("config_op_1.cfg", std::ios::trunc | std::ios::out);
  ofs_op_1
      << "listener = {"
         "   host = (\"127.0.0.1:10004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_1.close();

  std::ofstream ofs_op_2;
  ofs_op_2.open("config_op_2.cfg", std::ios::trunc | std::ios::out);
  ofs_op_2
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_2.close();

  std::ofstream ofs_ob;
  ofs_ob.open("config_ob.cfg", std::ios::trunc | std::ios::out);
  ofs_ob << "listener = {"
            "   host = (\"127.0.0.1:10003\")"
            "}"
            "box_channel_recv_buffer_len = 1024 "
            "box_name = \"test_box\""
            "connect_other_box_timeout = 2000 "
            "service_finder_connect_timeout = 2000 "
            "logger_config_line = "
            "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
            "open_coroutine = \"false\""
            "service_finder = {"
            "   hosts = \"127.0.0.1:2181\""
            "}"
            "necessary_service = (\"op_service\")";
  ofs_ob.close();

  std::ofstream ofs_ob_1;
  ofs_ob_1.open("config_ob_1.cfg", std::ios::trunc | std::ios::out);
  ofs_ob_1 << "listener = {"
              "   host = (\"127.0.0.1:10005\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service_finder = {"
              "   hosts = \"127.0.0.1:2181\""
              "}"
              "necessary_service = (\"dynamic\")";
  ofs_ob_1.close();

  std::ofstream ofs_ob_2;
  ofs_ob_2.open("config_ob_2.cfg", std::ios::trunc | std::ios::out);
  ofs_ob_2 << "listener = {"
              "   host = (\"127.0.0.1:11005\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service_finder = {"
              "   hosts = \"127.0.0.1:2181\""
              "}"
              "necessary_service = (\"dynamic\")";
  ofs_ob_2.close();

  std::ofstream ofs_bad;
  ofs_bad.open("config_bad.cfg", std::ios::trunc | std::ios::out);
  ofs_bad << "listener = {"
             "   host = (\"127.0.0.1:10003\")"
             "}"
             "box_channel_recv_buffer_len = 1024 "
             "box_name = \"test_box\""
             "connect_other_box_timeout = 2000 "
             "service_finder_connect_timeout = 2000 "
             "logger_config_line = "
             "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
             "open_coroutine = \"false\""
             "service_finder = {"
             "   hosts = \"127.0.0.1:2181\""
             "}"
             "necessary_service = 1"; // ���ʹ��������
  ofs_bad.close();

  std::ofstream ofs_bad_1;
  ofs_bad_1.open("config_bad_1.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_1 << "listener = {"
               "   host = (\"127.0.0.1\")" // ���������
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_1.close();

  std::ofstream ofs_bad_2;
  ofs_bad_2.open("config_bad_2.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_2 << "listener = {"
               "   host = (\"127.0.0.1:xxx\")" // ���������
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_2.close();

  std::ofstream ofs_bad_3;
  ofs_bad_3.open("config_bad_3.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_3 << "listener = {"
               "   host = (\"xxxxx:12345\")" // ���������
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_3.close();

  std::ofstream ofs_bad_4;
  ofs_bad_4.open("config_bad_4.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_4 << "listener = {"
               "   host = (\"127.0.0.1:10003\")"
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = \"sjkdfjksjfksjdfk\" " // ���������
               "open_coroutine = \"false\""
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}";
  ofs_bad_4.close();

  std::ofstream ofs_bad_5;
  ofs_bad_5.open("config_bad_5.cfg", std::ios::trunc | std::ios::out);
  ofs_bad_5 << "listener = {"
               "   host = (\"127.0.0.1:10001\")"
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\" "
               "service_finder = {"
               "   type = \"unknown\""
               "}";
  ofs_bad_5.close();

  std::ofstream ofs_hl_1;
  ofs_hl_1.open("ofs_hl_1.cfg", std::ios::trunc | std::ios::out);
  ofs_hl_1 << "listener = {"
              "   host = (\"127.0.0.1:11004\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service = { "
              "   service_dir = \"./service\" "
              "   remote_service_repo_version_api = "
              "\"http://127.0.0.1/doc/version.json\" "
              "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
              "   remote_service_repo_latest_version_api= "
              "\"http://127.0.0.1/doc/version.json\" "
              "   is_open_remote_update = \"true\" "
              "   remote_repo_check_interval = 1 "
              "}";
  ofs_hl_1.close();

  std::ofstream ofs_hl_wrong_version_api;
  ofs_hl_wrong_version_api.open("ofs_hl_wrong_version_api.cfg",
                                std::ios::trunc | std::ios::out);
  ofs_hl_wrong_version_api
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service = { "
         "   service_dir = \"./service\" "
         "   remote_service_repo_version_api = \"http://wrong_api\" "
         "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
         "   remote_service_repo_latest_version_api= \"http://wrong_api\" "
         "   is_open_remote_update = \"true\" "
         "   remote_repo_check_interval = 1 "
         "}";
  ofs_hl_wrong_version_api.close();

  std::ofstream ofs_hl_wrong_version_file;
  ofs_hl_wrong_version_file.open("ofs_hl_wrong_version_file.cfg",
                                 std::ios::trunc | std::ios::out);
  ofs_hl_wrong_version_file
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service = { "
         "   service_dir = \"./service\" "
         "   remote_service_repo_version_api = \"http://wrong_api\" "
         "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
         "   remote_service_repo_latest_version_api= "
         "\"http://127.0.0.1/doc/wrong_version.json\" "
         "   is_open_remote_update = \"true\" "
         "   remote_repo_check_interval = 1 "
         "}";
  ofs_hl_wrong_version_file.close();

  std::ofstream ofs_hl_wrong_version_bundle;
  ofs_hl_wrong_version_bundle.open("ofs_hl_wrong_version_bundle.cfg",
                                   std::ios::trunc | std::ios::out);
  ofs_hl_wrong_version_bundle
      << "listener = {"
         "   host = (\"127.0.0.1:11004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service = { "
         "   service_dir = \"./service\" "
         "   remote_service_repo_version_api = \"http://wrong_api\" "
         "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
         "   remote_service_repo_latest_version_api= "
         "\"http://127.0.0.1/doc/wrong_version_bundle.json\" "
         "   is_open_remote_update = \"true\" "
         "   remote_repo_check_interval = 1 "
         "}";
  ofs_hl_wrong_version_bundle.close();

  std::ofstream ofs_hl_2;
  ofs_hl_2.open("ofs_hl_2.cfg", std::ios::trunc | std::ios::out);
  ofs_hl_2 << "listener = {"
              "   host = (\"127.0.0.1:11004\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service = { "
              "   service_dir = \"./service\" "
              "   remote_service_repo_version_api = "
              "\"http://127.0.0.1/doc/version1.json\" "
              "   remote_service_repo_dir = \"http://127.0.0.1/doc/\" "
              "   is_open_remote_update = \"true\" "
              "   remote_repo_check_interval = 1 "
              "}";
  ofs_hl_2.close();
})
#endif

CASE(TestStart1) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test"};
  ASSERT_TRUE(sb.start(1, argv));
  sb.stop();
}

CASE(TestStart2) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "-h"};
  ASSERT_TRUE(!sb.start(2, argv));
  sb.stop();
}

CASE(TestStart3) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  ASSERT_TRUE(sb.get_argument().get_config_file_path() == "config.cfg");
  sb.update();
  sb.stop();
}

CASE(TestStart4) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  ASSERT_TRUE(!sb.try_get_transport("none"));
  sb.stop();
}

CASE(TestStart5) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  ASSERT_TRUE(!sb.get_transport_sync("none", 1));
  ASSERT_TRUE(!sb.is_wait_stop());
  sb.write_log_line(3, "test log line");
  sb.set_wait_stop_flag();
  ASSERT_TRUE(sb.is_wait_stop());
  sb.stop();
}

CASE(TestStart6) {
  kratos::service::ServiceBox op;
  kratos::service::ServiceBox ob;
  const char *argv_op[] = {"test_op", "--config=config_op.cfg"};
  const char *argv_ob[] = {"test_ob", "--config=config_ob.cfg"};
  ASSERT_TRUE(op.start(2, argv_op));
  auto start = kratos::util::get_os_time_millionsecond();
  while (true) {
    op.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 1000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  ASSERT_TRUE(op.register_service("op_service"));
  ASSERT_TRUE(!op.unregister_service("service_not_found"));
  ASSERT_TRUE(!op.unregister_service(""));
  ASSERT_TRUE(ob.start(2, argv_ob));
  start = kratos::util::get_os_time_millionsecond();
  while (true) {
    op.update();
    ob.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 2000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  ASSERT_TRUE(nullptr != ob.try_get_transport("op_service"));
  op.stop();
  ob.stop();
}

CASE(TestStart7) {
  kratos::service::ServiceBox op;
  kratos::service::ServiceBox ob;
  const char *argv_op[] = {"test_op", "--config=config_op.cfg"};
  const char *argv_ob[] = {"test_ob", "--config=config_ob.cfg"};
  ASSERT_TRUE(op.start(2, argv_op));
  auto start = kratos::util::get_os_time_millionsecond();
  while (true) {
    op.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 1000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  ASSERT_TRUE(op.register_service("op_service"));
  ASSERT_TRUE(ob.start(2, argv_ob));
  auto trans = ob.get_transport_sync("op_service", 1000);
  ASSERT_TRUE(nullptr != trans);
  if (trans) {
      trans->close();
  }
  start = kratos::util::get_os_time_millionsecond();
  while (true) {
    op.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 1000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  trans = ob.get_transport_sync("op_service", 1000);
  ASSERT_TRUE(nullptr != trans);
  op.stop();
  ob.stop();
}

CASE(TestStart8) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "sjdkfjkdjf"};
  ASSERT_TRUE(sb.start(2, argv));
  sb.stop();
}

CASE(TestStart9) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config_bad.cfg"};
  ASSERT_TRUE(!sb.start(2, argv));
  sb.stop();
}

CASE(TestStart10) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config_bad_1.cfg"};
  ASSERT_TRUE(!sb.start(2, argv));
  sb.stop();
}

CASE(TestStart11) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config_bad_2.cfg"};
  ASSERT_TRUE(!sb.start(2, argv));
  sb.stop();
}

CASE(TestStart12) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config_bad_3.cfg"};
  ASSERT_TRUE(!sb.start(2, argv));
  sb.stop();
}

CASE(TestStart13) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config_bad_4.cfg"};
  ASSERT_TRUE(!sb.start(2, argv));
  sb.stop();
}

CASE(TestStart14) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  ASSERT_TRUE(!sb.register_service(""));
  ASSERT_TRUE(!sb.unregister_service(""));
  sb.stop();
}

CASE(TestStart15) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config_bad_5.cfg"};
  ASSERT_TRUE(!sb.start(2, argv));
  sb.stop();
}

CASE(TestStart16) {
  kratos::service::ServiceBox op;
  const char *argv_op[] = {"test_op", "--config=config_op.cfg"};
  ASSERT_TRUE(op.start(2, argv_op));
  ASSERT_TRUE(op.register_service("/never/first/1"));
  op.stop();
}

CASE(TestStart17) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config_op_1.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  sb.stop();
}

CASE(TestStart18) {
  kratos::service::ServiceBox op;
  kratos::service::ServiceBox ob;
  const char *argv_op[] = {"test", "--config=config_op_1.cfg"};
  const char *argv_ob[] = {"test", "--config=config_ob_1.cfg"};
  ASSERT_TRUE(op.start(2, argv_op));
  op.register_service("dynamic");
  ASSERT_TRUE(op.register_service("/nothing/abc"));
  ASSERT_TRUE(ob.start(2, argv_ob));
  auto start = kratos::util::get_os_time_millionsecond();
  while (true) {
    op.update();
    ob.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 1000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  auto trans = ob.get_transport_sync("dynamic", 1);
  ASSERT_TRUE(trans != nullptr);
  op.stop();
  ob.stop();
}

CASE(TestStart19) {
  kratos::service::ServiceBox op;
  kratos::service::ServiceBox ob;
  const char *argv_op[] = {"test", "--config=config_op_1.cfg"};
  const char *argv_ob[] = {"test", "--config=config_ob_1.cfg"};
  ASSERT_TRUE(op.start(2, argv_op));
  op.register_service("dynamic");
  ASSERT_TRUE(ob.start(2, argv_ob));
  auto start = kratos::util::get_os_time_millionsecond();
  while (true) {
    op.update();
    ob.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 1000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  auto trans = ob.try_get_transport("dynamic");
  ASSERT_TRUE(trans != nullptr);
  op.stop();
  ob.stop();
}

CASE(TestStart20) {
  kratos::service::ServiceBox op;
  kratos::service::ServiceBox ob;
  const char *argv_op[] = {"test", "--config=config_op_2.cfg"};
  const char *argv_ob[] = {"test", "--config=config_ob_2.cfg"};
  ASSERT_TRUE(op.start(2, argv_op));
  op.register_service("dynamic");
  ASSERT_TRUE(ob.start(2, argv_ob));
  auto start = kratos::util::get_os_time_millionsecond();
  while (true) {
    op.update();
    ob.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 1000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  auto trans = ob.try_get_transport("dynamic");
  auto prx = rpc::getService<ServiceDynamicProxy>(trans, false, ob.get_rpc());
  ASSERT_TRUE(trans != nullptr);
  bool call = false;
  if (prx) {
    prx->method5([&](rpc::RpcError) { call = true; });
  }
  bool co_call = false;
  co(
      coro_sleep(200);
      if (prx){
          try {
              prx->method5();
          } catch (...) {
          }
      }
      co_call = true;
  )
  start = kratos::util::get_os_time_millionsecond();
  while (true) {
    op.update();
    ob.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 1000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  ASSERT_TRUE(call);
  ASSERT_TRUE(co_call);
  op.stop();
  ob.stop();
}

CASE(TestStart21) {
  kratos::service::ServiceBox op;
  kratos::service::ServiceBox ob;
  const char *argv_op[] = {"test", "--config=config_op_1.cfg"};
  const char *argv_ob[] = {"test", "--config=config_ob_1.cfg"};
  ASSERT_TRUE(op.start(2, argv_op));
  op.register_service("dynamic");
  ASSERT_TRUE(ob.start(2, argv_ob));
  auto start = kratos::util::get_os_time_millionsecond();
  while (true) {
    op.update();
    ob.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 1000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  kratos::service::ServiceContextImpl ctx(&ob);
  std::shared_ptr<ServiceDynamicProxy> prx;
  auto id = co(prx = ctx.get_proxy_co<ServiceDynamicProxy>("dynamic", 1000);
     ASSERT_TRUE(prx != nullptr);) ASSERT_TRUE(prx != nullptr);
  auto prx1 = ctx.try_get_proxy<ServiceDynamicProxy>("dynamic");
  ASSERT_TRUE(prx1 != nullptr);
  auto prx2 = ctx.get_proxy_sync<ServiceDynamicProxy>("dynamic", 1000);
  ASSERT_TRUE(prx2 != nullptr);
  ctx.get_argument();
  ctx.write_log_line(1, "ctx test log line");
  ctx.get_config();
  coro_close(id);
  op.stop();
  ob.stop();
}

CASE(TestStart22) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config_op_1.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  sb.stop();
}

CASE(TestStart23) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=ofs_hl_1.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  sb.stop();
}

CASE(TestStart24) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=ofs_hl_wrong_version_api.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  sb.stop();
}

CASE(TestStart25) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=ofs_hl_wrong_version_file.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  sb.stop();
}

CASE(TestStart26) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=ofs_hl_wrong_version_bundle.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  sb.update();
  sb.stop();
}

CASE(TestStart27) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=ofs_hl_2.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  auto start = kratos::util::get_os_time_millionsecond();
  while (true) {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    sb.update();
    if (kratos::util::get_os_time_millionsecond() - start > 5000) {
      break;
    }
  }
  sb.stop();
}

CASE(TestStart28) {
  kratos::service::ServiceBox sb;
  const char *argv[] = {"test", "--config=config.cfg"};
  ASSERT_TRUE(sb.start(2, argv));
  kratos::service::ServiceContextImpl ctx(&sb);
  std::shared_ptr<ServiceDynamicProxy> prx;
  auto start = kratos::util::get_os_time_millionsecond();
  auto id = co(prx =
         ctx.get_proxy_co<ServiceDynamicProxy>("unknown", 100);) while (true) {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    sb.update();
    if (kratos::util::get_os_time_millionsecond() - start > 2000) {
      break;
    }
  }
  ASSERT_TRUE(!prx);
  coro_close(id);
  sb.stop();
}

TEARDOWN([]() {
  kratos::util::remove_file("box.cfg");
  kratos::util::remove_file("config.cfg");
  kratos::util::remove_file("config_op.cfg");
  kratos::util::remove_file("config_ob.cfg");
  kratos::util::remove_file("config_bad.cfg");
  kratos::util::remove_file("config_bad_1.cfg");
  kratos::util::remove_file("config_bad_2.cfg");
  kratos::util::remove_file("config_bad_3.cfg");
  kratos::util::remove_file("config_bad_4.cfg");
  kratos::util::remove_file("config_bad_5.cfg");
  kratos::util::remove_file("config_op_1.cfg");
  kratos::util::remove_file("config_ob_1.cfg");
  kratos::util::remove_file("config_op_2.cfg");
  kratos::util::remove_file("config_ob_2.cfg");
  kratos::util::remove_file("ofs_hl_1.cfg");
  kratos::util::remove_file("service/libServiceDynamic.so");
  kratos::util::remove_file("version1.0/version.json");
  kratos::util::remove_file("ofs_hl_wrong_version_api.cfg");
  kratos::util::remove_file("ofs_hl_wrong_version_file.cfg");
  kratos::util::remove_file("ofs_hl_wrong_version_bundle.cfg");
  kratos::util::remove_file("ofs_hl_2.cfg");
  kratos::util::remove_file("service/temp_bundle_dir/version2.0.done");
  kratos::util::remove_file(
      "service/temp_bundle_dir/version2.0/libServiceDynamic.so");
  kratos::util::rm_empty_dir("service/temp_bundle_dir/version2.0/");
  kratos::util::rm_empty_dir("service/temp_bundle_dir/");
  kratos::util::rm_empty_dir("service");
})

FIXTURE_END(test_service_box)

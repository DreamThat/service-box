import os
import shutil
import json
import platform
import sys
import subprocess
import glob
from driver import Initializer
from driver import Builder
from util import *
from option import Options

class Initializer_csharp(Initializer):
    def __init__(self, options):
        self.options = options

    def check(self):
        if not os.path.exists("repo.init"):
            print("Base repo. uninitialized.")
            return False
        if not os.path.exists("repo.init.csharp"):
            return self.init()
        else:
            return True

    def build_frontend(self):
        pass
        
    def build_backend(self):
        pass
        
    def init(self):
        os.chdir("tmp")
        if os.path.exists('rpc-backend-csharp'):
            os.chdir("rpc-backend-csharp")
            os.system("git pull")
            os.chdir('..')
        else:
            if os.system("git clone https://gitee.com/dennis-kk/rpc-backend-csharp") != 0:
                print("git clone https://gitee.com/dennis-kk/rpc-backend-csharp failed")
                return False
        shutil.copyfile("rpc-backend-csharp/rpc/tools/csharpgen.py", "../bin/csharpgen.py")
        shutil.copyfile("rpc-backend-csharp/rpc/tools/csharpgen_pb_layer.py", "../bin/csharpgen_pb_layer.py")
        os.chdir("..")
        open("repo.init.csharp", "w").close()
        if not os.path.exists('usr/csharp'):
            os.makedirs('usr/csharp')
        return True
        
    def checkEnv(self):
        os.system("protoc --version")
        os.system("git --version")
        os.system("cmake --version")
        if platform.system() == "Linux":
            if os.system("gcc --version") != 0:
                os.system("clang --version")
        else:
            print("Visual Studio 16 2019")

class Builder_csharp(Builder):
    def __init__(self, options):
        Builder.__init__(self, options)

    def build_idl(self, name, sname = None):
        pass
        
    def addIdl2Repo(self, file_name, sname = None, add=True):
        if not add:
            file_name = "src/idl/" + file_name
        if not os.path.exists(file_name):
            print(file_name + " not found")
            return
        file_name = self.check_idl_name(file_name)
        if add:
            shutil.copyfile(file_name, "src/idl/" + os.path.basename(file_name))
        os.chdir("src/idl/")
        cmd = subprocess.Popen(["../../bin/rpc-frontend", "-t", "csharp", "-f", os.path.basename(file_name)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        ret = cmd.communicate()
        if cmd.returncode != 0:
            print(ret[1])
            return
        (base_name, _) = os.path.splitext(os.path.basename(file_name))
        cmd = subprocess.Popen(["python", "../../bin/csharpgen.py", base_name+'.idl.csharp.json'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        cmd.communicate()
        if cmd.returncode != 0:
            print("csharpgen.py failed")
            print(ret[1])
            return
        cmd = subprocess.Popen(["python", "../../bin/csharpgen_pb_layer.py", base_name+'.idl.csharp.json', base_name+'.idl.protobuf.json'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        cmd.communicate()
        if cmd.returncode != 0:
            print("csharpgen.py failed")
            print(ret[1])
            return
        json_str = json.load(open(base_name + ".idl.cpp.json"))
        if not os.path.exists('../../usr/csharp/'+base_name):
            os.makedirs('../../usr/csharp/'+base_name)
        shutil.copyfile('rpc.register.cs', '../../usr/csharp/rpc.register.cs')
        shutil.copyfile(base_name+'.service.cs', '../../usr/csharp/'+base_name+'/'+base_name+'.service.cs')
        shutil.copyfile(base_name.capitalize()+'Service.cs', '../../usr/csharp/'+base_name+'/'+base_name.capitalize()+'Service.cs')
        shutil.copyfile(base_name+'.service.protobuf.serializer.cs', '../../usr/csharp/'+base_name+'/'+base_name+'.service.protobuf.serializer.cs')
        for service in json_str['services']:
            if not os.path.exists('../../usr/csharp/'+base_name+'/'+service['name']):
                os.makedirs('../../usr/csharp/'+base_name+'/'+service['name'])
            stub_file_name = '../../usr/csharp/'+base_name+'/'+service['name']+'/'+base_name+'.service.'+service['name']+'.impl.cs'
            inter_file_name = '../../usr/csharp/'+base_name+'/'+service['name']+'/'+base_name+'.service.'+service['name']+'.cs'
            if not os.path.exists(stub_file_name) and os.path.exists(base_name+'.service.'+service['name']+'.impl.cs'):
                shutil.copyfile(base_name+'.service.'+service['name']+'.impl.cs', stub_file_name)
            if os.path.exists(base_name+'.service.'+service['name']+'.cs'):
                shutil.copyfile(base_name+'.service.'+service['name']+'.cs', inter_file_name)
        os.chdir("../../")
        
    def updateRoot(self):
        initializer = Initializer_csharp(Options())
        os.chdir("tmp/rpc-frontend/")
        os.system("git pull")
        os.chdir("..")
        initializer.build_frontend()
        os.chdir("..")
        os.chdir("tmp/rpc-backend-csharp/")
        os.system("git pull")
        shutil.copyfile("rpc/tools/csharpgen.py", "../../bin/csharpgen.py")
        shutil.copyfile("rpc/tools/csharpgen_pb_layer.py", "../../bin/csharpgen_pb_layer.py")
        os.chdir("..")
        initializer.build_backend()
        os.chdir("../../")
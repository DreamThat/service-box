/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "array_impl.h"

ArrayImpl::ArrayImpl() {}

ArrayImpl::~ArrayImpl() {
  std::vector<Attribute *>::iterator iterator = _attributes.begin();

  for (; iterator != _attributes.end(); iterator++) {
    delete (*iterator);
  }

  _attributes.clear();
}

int ArrayImpl::getSize() { return (int)_attributes.size(); }

NumberAttribute *ArrayImpl::number(int index) {
  if (index < (int)_attributes.size()) {
    Attribute *attribute = _attributes.at(index);

    if (attribute->isNumber()) {
      return dynamic_cast<NumberAttribute *>(attribute);
    }
  }

  throw ConfigException("invalid type conversion");
}

StringAttribute *ArrayImpl::string(int index) {
  if (index < (int)_attributes.size()) {
    Attribute *attribute = _attributes.at(index);

    if (attribute->isString()) {
      return dynamic_cast<StringAttribute *>(attribute);
    }
  }

  throw ConfigException("invalid type conversion");
}

ArrayAttribute *ArrayImpl::array(int index) {
  if (index < (int)_attributes.size()) {
    Attribute *attribute = _attributes.at(index);

    if (attribute->isArray()) {
      return dynamic_cast<ArrayAttribute *>(attribute);
    }
  }

  throw ConfigException("invalid type conversion");
}

TableAttribute *ArrayImpl::table(int index) {
  if (index < (int)_attributes.size()) {
    Attribute *attribute = _attributes.at(index);

    if (attribute->isTable()) {
      return dynamic_cast<TableAttribute *>(attribute);
    }
  }

  throw ConfigException("invalid type conversion");
}

Attribute *ArrayImpl::get(int index) {
  if (index < (int)_attributes.size()) {
    return _attributes.at(index);
  }
  return nullptr;
}

void ArrayImpl::push(Attribute *attribute) { _attributes.push_back(attribute); }

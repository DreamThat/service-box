#include "../../src//repo/src/include/example/Login/proxy/example.service.Login.proxy.h"
#include "../../src//repo/src/include/example/ServiceDynamic/proxy/example.service.ServiceDynamic.proxy.h"
#include "../../src/argument/box_argument.hh"
#include "../../src/box/service_box.hh"
#include "../../src/box/service_http_loader.hh"
#include "../../src/detail/service_context_impl.hh"
#include "../../src/repo/src/include/root/rpc_root.h"
#include "../../src/util/os_util.hh"
#include "../../src/util/time_util.hh"
#include "../framework/unittest.hh"
#include <fstream>

FIXTURE_BEGIN(test_proxy)

#ifdef WIN32
#ifdef _DEBUG
SETUP([]() {
  kratos::util::make_dir("service");
  std::ofstream ofs;
  ofs.open("config.cfg", std::ios::trunc | std::ios::out);
  ofs << "listener = {"
         "   host = (\"127.0.0.1:10001\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\"";
  ofs.close();

  std::ofstream ofs1;
  ofs1.open("config1.cfg", std::ios::trunc | std::ios::out);
  ofs1 << "listener = {"
          "   host = (\"127.0.0.1:10001\")"
          "}"
          "box_channel_recv_buffer_len = 1024 "
          "box_name = \"test_box\""
          "connect_other_box_timeout = 2000 "
          "service_finder_connect_timeout = 2000 "
          "logger_config_line = "
          "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
          "open_coroutine = \"false\" "
          "service = {"
          "   service_dir = "
          "\"../../../src/repo/lib/stub/example/Login/Debug\" "
          "   preload_service = <5871407834537456905:\"libLogin.so\">"
          "}";
  "proxy = { listener=(\"127.0.0.1:10101\") seed=1 query_timeout=1000 }";
  ofs1.close();

  std::ofstream ofs_proxy;
  ofs_proxy.open("proxy.cfg", std::ios::trunc | std::ios::out);
  ofs_proxy
      << "listener = {"
         "   host = (\"127.0.0.1:11001\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}  "
         "proxy = { listener=(\"127.0.0.1:10101\") seed=1 query_timeout=1000 }";
  ofs_proxy.close();

  std::ofstream ofs_proxy1;
  ofs_proxy1.open("proxy1.cfg", std::ios::trunc | std::ios::out);
  ofs_proxy1
      << "listener = {"
         "   host = (\"127.0.0.1:11001\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "necessary_service = (\"5871407834711899994\") "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}  "
         "proxy = { listener=(\"127.0.0.1:10101\") seed=1 query_timeout=1000 }";
  ofs_proxy1.close();

  std::ofstream ofs_op_1;
  ofs_op_1.open("config_op_1.cfg", std::ios::trunc | std::ios::out);
  ofs_op_1
      << "listener = {"
         "   host = (\"127.0.0.1:10004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic/Debug\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_1.close();

  std::ofstream ofs_op_2;
  ofs_op_2.open("config_op_2.cfg", std::ios::trunc | std::ios::out);
  ofs_op_2 << "listener = {"
              "   host = (\"127.0.0.1:10004\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service_finder = {"
              "   hosts = \"127.0.0.1:2181\""
              "}";
  ofs_op_2.close();

  std::ofstream ofs_op_coro;
  ofs_op_coro.open("config_op_coro.cfg", std::ios::trunc | std::ios::out);
  ofs_op_coro
      << "listener = {"
         "   host = (\"127.0.0.1:10004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"true\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic/Debug\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_coro.close();
})
#else
SETUP([]() {
  kratos::util::make_dir("service");
  std::ofstream ofs;
  ofs.open("config.cfg", std::ios::trunc | std::ios::out);
  ofs << "listener = {"
         "   host = (\"127.0.0.1:10001\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\"";
  ofs.close();

  std::ofstream ofs_proxy;
  ofs_proxy.open("proxy.cfg", std::ios::trunc | std::ios::out);
  ofs_proxy << "listener = {"
               "   host = (\"127.0.0.1:11001\")"
               "}"
               "box_channel_recv_buffer_len = 1024 "
               "box_name = \"test_box\""
               "connect_other_box_timeout = 2000 "
               "service_finder_connect_timeout = 2000 "
               "logger_config_line = "
               "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
               "open_coroutine = \"false\" "
               "service_finder = {"
               "   hosts = \"127.0.0.1:2181\""
               "}  "
               "proxy = { listener=(\"127.0.0.1:10101\")  seed=1 "
               "query_timeout=1000 }";
  ofs_proxy.close();

  std::ofstream ofs1;
  ofs1.open("config1.cfg", std::ios::trunc | std::ios::out);
  ofs1
      << "listener = {"
         "   host = (\"127.0.0.1:10001\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/Login/Release\" "
         "   preload_service = <5871407834537456905:\"libLogin.so\">"
         "}"
         "proxy = { listener=(\"127.0.0.1:10101\") seed=1 query_timeout=1000 }";
  ofs1.close();

  std::ofstream ofs_proxy1;
  ofs_proxy1.open("proxy1.cfg", std::ios::trunc | std::ios::out);
  ofs_proxy1
      << "listener = {"
      "   host = (\"127.0.0.1:11001\")"
      "}"
      "box_channel_recv_buffer_len = 1024 "
      "box_name = \"test_box\""
      "connect_other_box_timeout = 2000 "
      "service_finder_connect_timeout = 2000 "
      "logger_config_line = "
      "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
      "open_coroutine = \"false\" "
      "necessary_service = (\"5871407834711899994\") "
      "service_finder = {"
      "   hosts = \"127.0.0.1:2181\""
      "}  "
      "proxy = { listener=(\"127.0.0.1:10101\") seed=1 query_timeout=1000 }";
  ofs_proxy1.close();

  std::ofstream ofs_op_1;
  ofs_op_1.open("config_op_1.cfg", std::ios::trunc | std::ios::out);
  ofs_op_1
      << "listener = {"
         "   host = (\"127.0.0.1:10004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic/Release\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_1.close();

  std::ofstream ofs_op_2;
  ofs_op_2.open("config_op_2.cfg", std::ios::trunc | std::ios::out);
  ofs_op_2 << "listener = {"
              "   host = (\"127.0.0.1:10004\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service_finder = {"
              "   hosts = \"127.0.0.1:2181\""
              "}";
  ofs_op_2.close();

  std::ofstream ofs_op_coro;
  ofs_op_coro.open("config_op_coro.cfg", std::ios::trunc | std::ios::out);
  ofs_op_coro
      << "listener = {"
         "   host = (\"127.0.0.1:10004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"true\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic/Release\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_coro.close();
})
#endif // _DEBUG
#else
SETUP([]() {
  kratos::util::make_dir("service");
  std::ofstream ofs;
  ofs.open("config.cfg", std::ios::trunc | std::ios::out);
  ofs << "listener = {"
         "   host = (\"127.0.0.1:10001\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\"";
  ofs.close();

  std::ofstream ofs_proxy;
  ofs_proxy.open("proxy.cfg", std::ios::trunc | std::ios::out);
  ofs_proxy
      << "listener = {"
         "   host = (\"127.0.0.1:11001\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}  "
         "proxy = { listener=(\"127.0.0.1:10101\") seed=1 query_timeout=1000 }";
  ofs_proxy.close();

  std::ofstream ofs1;
  ofs1.open("config1.cfg", std::ios::trunc | std::ios::out);
  ofs1
      << "listener = {"
         "   host = (\"127.0.0.1:10001\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/Login/Debug\" "
         "   preload_service = <5871407834537456905:\"libLogin.so\">"
         "}"
         "proxy = { listener=(\"127.0.0.1:10101\") seed=1 query_timeout=1000 }";
  ofs1.close();

  std::ofstream ofs_op_1;
  ofs_op_1.open("config_op_1.cfg", std::ios::trunc | std::ios::out);
  ofs_op_1
      << "listener = {"
         "   host = (\"127.0.0.1:10004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"false\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic/Debug\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_1.close();

  std::ofstream ofs_op_2;
  ofs_op_2.open("config_op_2.cfg", std::ios::trunc | std::ios::out);
  ofs_op_2 << "listener = {"
              "   host = (\"127.0.0.1:10004\")"
              "}"
              "box_channel_recv_buffer_len = 1024 "
              "box_name = \"test_box\""
              "connect_other_box_timeout = 2000 "
              "service_finder_connect_timeout = 2000 "
              "logger_config_line = "
              "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
              "open_coroutine = \"false\" "
              "service_finder = {"
              "   hosts = \"127.0.0.1:2181\""
              "}";
  ofs_op_2.close();

  std::ofstream ofs_op_coro;
  ofs_op_coro.open("config_op_coro.cfg", std::ios::trunc | std::ios::out);
  ofs_op_coro
      << "listener = {"
         "   host = (\"127.0.0.1:10004\")"
         "}"
         "box_channel_recv_buffer_len = 1024 "
         "box_name = \"test_box\""
         "connect_other_box_timeout = 2000 "
         "service_finder_connect_timeout = 2000 "
         "logger_config_line = "
         "\"console://;line=[%y%m%d-%H:%M:%S:%U];flush=true\" "
         "open_coroutine = \"true\" "
         "service_finder = {"
         "   hosts = \"127.0.0.1:2181\""
         "}"
         "service = {"
         "   service_dir = "
         "\"../../../src/repo/lib/stub/example/ServiceDynamic/Debug\" "
         "   preload_service = <5871407834711899994:\"libServiceDynamic.so\">"
         "}";
  ofs_op_coro.close();
})
#endif

CASE(TestProxyCall1) {
  kratos::service::ServiceBox caller;
  const char *argv[] = {"test", "--config=config.cfg",
                        "--proxy-host=127.0.0.1:10101"};
  ASSERT_TRUE(caller.start(3, argv));

  kratos::service::ServiceBox server;
  const char *argv2[] = {"test", "--config=config_op_1.cfg"};
  ASSERT_TRUE(server.start(2, argv2));

    kratos::service::ServiceBox proxy;
  const char *argv1[] = {"test", "--config=proxy1.cfg", "--proxy"};
  ASSERT_TRUE(proxy.start(3, argv1));

  auto start = kratos::util::get_os_time_millionsecond();
  while (true) {
    caller.update();
    proxy.update();
    server.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 2000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }

  auto trans = caller.try_get_transport("any");
  bool done = false;
  auto prx = rpc::getService<ServiceDynamicProxy>(trans, true, caller.get_rpc());
  if (prx) {
    prx->method2(1, {}, 2, [&](const std::string &, rpc::RpcError error) {
      done = (error == rpc::RpcError::OK);
    });
  }
  start = kratos::util::get_os_time_millionsecond();
  while (true) {
    caller.update();
    proxy.update();
    server.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 5000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }
  ASSERT_TRUE(done);
  proxy.stop();
  caller.stop();
  server.stop();
}

CASE(TestProxyCall2) {
  kratos::service::ServiceBox caller;
  const char *argv[] = {"test", "--config=config.cfg",
                        "--proxy-host=127.0.0.1:10101"};
  ASSERT_TRUE(caller.start(3, argv));

  kratos::service::ServiceBox server;
  const char *argv2[] = {"test", "--config=config_op_2.cfg"};
  ASSERT_TRUE(server.start(2, argv2));

  kratos::service::ServiceBox proxy;
  const char *argv1[] = {"test", "--config=proxy.cfg", "--proxy"};
  ASSERT_TRUE(proxy.start(3, argv1));

  auto start = kratos::util::get_os_time_millionsecond();
  while (true) {
    caller.update();
    proxy.update();
    server.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 1000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }

  auto trans = caller.try_get_transport("any");
  bool done = false;
  auto prx = rpc::getService<ServiceDynamicProxy>(trans, true, caller.get_rpc());
  if (prx) {
    prx->method2(1, {}, 2, [&](const std::string &, rpc::RpcError error) {
      done = (error == rpc::RpcError::NOT_FOUND);
    });
  }
  start = kratos::util::get_os_time_millionsecond();
  while (true) {
    caller.update();
    proxy.update();
    server.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 5000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }
  ASSERT_TRUE(done);
  proxy.stop();
  server.stop();
  caller.stop();
}

CASE(TestProxyCall3) {
  kratos::service::ServiceBox caller;
  const char *argv[] = {"test", "--config=config1.cfg",
                        "--proxy-host=127.0.0.1:10101"};
  ASSERT_TRUE(caller.start(3, argv));

  kratos::service::ServiceBox server;
  const char *argv2[] = {"test", "--config=config_op_1.cfg"};
  ASSERT_TRUE(server.start(2, argv2));

  kratos::service::ServiceBox proxy;
  const char *argv1[] = {"test", "--config=proxy1.cfg", "--proxy"};
  ASSERT_TRUE(proxy.start(3, argv1));

  auto start = kratos::util::get_os_time_millionsecond();
  while (true) {
    caller.update();
    proxy.update();
    server.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 5000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }

  auto trans = caller.try_get_transport("any");
  bool done = false;
  auto prx = rpc::getService<ServiceDynamicProxy>(trans, true, caller.get_rpc());
  int count = 0;
  int max_count = 10;
  if (prx) {
    for (int i = 0; i < max_count; i++) {
      prx->method2(1, {}, 2, [&](const std::string &, rpc::RpcError error) {
        done = (error == rpc::RpcError::OK);
        count += 1;
      });
    }
  }
  start = kratos::util::get_os_time_millionsecond();
  while (true) {
    caller.update();
    proxy.update();
    server.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 5000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }
  ASSERT_TRUE(done);
  ASSERT_TRUE(count == max_count);
  proxy.stop();
  server.stop();
  caller.stop();
}

CASE(TestProxyCall4) {
  kratos::service::ServiceBox caller;
  const char *argv[] = {"test", "--config=config1.cfg",
                        "--proxy-host=127.0.0.1:10101"};
  ASSERT_TRUE(caller.start(3, argv));

  kratos::service::ServiceBox server;
  const char *argv2[] = {"test", "--config=config_op_coro.cfg"};
  ASSERT_TRUE(server.start(2, argv2));

  kratos::service::ServiceBox proxy;
  const char *argv1[] = {"test", "--config=proxy1.cfg", "--proxy"};
  ASSERT_TRUE(proxy.start(3, argv1));

  auto start = kratos::util::get_os_time_millionsecond();
  while (true) {
    caller.update();
    proxy.update();
    server.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 5000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }

  auto trans = caller.try_get_transport("any");
  bool done = false;
  auto prx = rpc::getService<ServiceDynamicProxy>(trans, true, caller.get_rpc());
  int count = 0;
  int max_count = 10;
  if (prx) {
    for (int i = 0; i < max_count; i++) {
      prx->method2(1, {}, 2, [&](const std::string &, rpc::RpcError error) {
        done = (error == rpc::RpcError::OK);
        count += 1;
      });
    }
  }
  start = kratos::util::get_os_time_millionsecond();
  while (true) {
    caller.update();
    proxy.update();
    server.update();
    auto now = kratos::util::get_os_time_millionsecond();
    if (now - start > 5000) {
      break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }
  ASSERT_TRUE(done);
  ASSERT_TRUE(count == max_count);
  proxy.stop();
  server.stop();
  caller.stop();
}

TEARDOWN([]() {
  kratos::util::remove_file("config.cfg");
  kratos::util::remove_file("proxy.cfg");
  kratos::util::remove_file("proxy1.cfg");
  kratos::util::remove_file("config1.cfg");
  kratos::util::remove_file("config_op_1.cfg");
  kratos::util::remove_file("config_op_2.cfg");
  kratos::util::remove_file("config_op_coro.cfg");
  kratos::util::rm_empty_dir("service");
})

FIXTURE_END(test_proxy)

## 安装准备

1. 需要安装支持C++17标准的编译工具链，Windows下推荐Visual studio 2019, Linux下推荐支持C++17标准的编译工具链
2. 需要安装git
3. 需要安装cmake版本 >= 3.5
4. 需要安装Protobuf3.0

### 安装Protobuf3

下载官方源代码包，版本要求必须支持3.0版本语法，使用cmake编译
#### Windows

在源代码根目录下：
```
cd cmake
cmake -G "Visual Studio 16 2019" -A x64
cmake --build . --config "Debug"
cmake --build . --config "Release"
```
#### Linux
依据官方文档安装即可

### 依赖参考
1. [协程](https://gitee.com/dennis-kk/coroutine/blob/master/README.md)
2. [IDL前端](https://gitee.com/dennis-kk/rpc-frontend/blob/master/README.md)
3. [IDL后端 C++](https://gitee.com/dennis-kk/rpc-backend-cpp/blob/master/README.md)

## 仓库

下载发行版[发行版下载](https://gitee.com/dennis-kk/rpc-repo/releases)

### 初始化仓库

```
python repo.py --init
```

### 添加接口定义文件
以自带的example/example.idl为例

```
python repo.py -t [type] -a example/example.idl

type支持的类型:
1. cpp c++服务
2. lua lua服务
3. csharp c#服务

多种类型的服务可以生成在同一个仓库下，针对每一种语言类型服务需要分别运行上面的命令

```
[IDL文件参考](https://gitee.com/dennis-kk/rpc-frontend/blob/master/README.md)

### 编译

```
python repo.py -t [type] -b example
```

#### 编译指定服务

```
python repo.py -t [type] -b example:Service
```

### 删除接口文件
同时产出所有与之相关内容

```
python repo.py -d example
```

### 更新接口文件

```
python repo.py -t [type] -u example
```

### 更新指定服务

```
python repo.py -t [type] -u example:Service
```

## 使用Mock
mock目录的内容，作为已有系统接入的一个简单例子，同时提供基于example/example.idl接口文件的性能测试程序
### 编译Mock
#### Windows
```
cd mock
cmake -G "Visual Studio 16 2019" -A x64
cmake --build . --config "Release"
cmake --build . --config "Debug"
```
#### Linux

```
cd mock/thirdparty/knet
cmake .
make
cd ../..
cd mock
cmake .
make
```
会生成四个可执行文件：
1. mock 单进程内echo测试
2. proxy RPC客户端测试程序，所有方法公用一个协程
3. proxy_co RPC客户端测试程序，每个方法开启一个协程
3. server RPC服务端测试程序，未开启协程
4. server_co RPC服务端测试程序，开启协程

### 运行Mock
#### Windows

```
cd Release
mock.exe

cd Debug
mock.exe
```
启动服务端

```
server.exe -h 127.0.0.1 -p 3333
server_co.exe -h 127.0.0.1 -p 3333
```
启动客户端
```
proxy.exe -h 127.0.0.1 -p 3333
proxy_co.exe -h 127.0.0.1 -p 3333
```

#### Linux

```
./mock
```
启动服务端

```
./server -h 127.0.0.1 -p 3333
./server_co -h 127.0.0.1 -p 3333
```
启动客户端
```
./proxy -h 127.0.0.1 -p 3333
./proxy_co -h 127.0.0.1 -p 3333
```

#### Mock代码
Mock实现了如下功能：
1. 代理程序调用example.idl所定义的接口方法
2. 服务器程序加载了example.idl中定义的两个服务，两个服务方法完全一样，区别是一个是静态编译到服务器程序中，另一个是被动态加载到服务器程程序中
代理程序调用服务的代码在mock/src/mock_caller.cpp中：
服务程序中对example.idl内服务的实现在仓库根目录下的：
1. usr/impl/example/Service/ 静态服务
2. usr/impl/example/ServiceDynamic/ 动态服务

## 目录结构
仓库初始化化后，目录结构至少包含如下的文件及目录：
+ example 示例IDL文件
+ mock 使用者例子工程及性能测试
+ usr 用户服务实现文件
+ cpp_driver.py C++ RPC框架编译脚本
+ driver.py 脚本生成驱动
+ repo.init C++初始化文件
+ repo.init.csharp csharp初始化文件
+ repo.init.lua lua初始化文件
+ repo.py 仓库脚本
+ util.py 工具脚本

#pragma once

#include <ctime>
#include <string>
#include <vector>

namespace kratos {
namespace util {

// transmit from domain to IP
// @param host domain host name
// @return IP
extern std::string get_host_ip(const std::string &host);
// returns the absobulte file path of executable binary
// @return the absobulte file path of executable binary
extern std::string get_binary_path();
// returns the executable binary file name
extern std::string get_binary_name();
// Collect all files in directory
// @param directory Directory
// @param suffix File extension
// @param fileNames result
// @retval true
// @retval false fail
extern bool get_file_in_directory(const std::string &directory,
                                  const std::string suffix,
                                  std::vector<std::string> &fileNames);
// Check existence of directory
// @param path Directory
// @retval true
// @retval false
extern bool is_path_exists(const std::string &path);
// Remove file
// @param filePath The file path
// @retval true
// @retval false
extern bool remove_file(const std::string &filePath);
// Completes the path
// @param base the base directory
// @param sub sub-directory or file name
// @return the joined path
extern std::string complete_path(const std::string &base,
                                 const std::string &sub);
// Completes the URL path
// @param base the base directory
// @param sub sub-directory or file name
// @return the joined path
extern std::string complete_path_url(const std::string &base,
                                     const std::string &sub);
// Renames the file
// @param srcFileName old file name
// @param newFileName new file name
// @retval true
// @retval false
extern bool rename_file(const std::string &srcFileName,
                        const std::string &newFileName);
// Returns file's suffix
// @param fileName The file path
extern std::string get_file_ext(const std::string &fileName);
// Returns the random number between a and b
extern std::uint32_t get_random_uint32(std::uint32_t a, std::uint32_t b);
// 建立目录
extern auto make_dir(const std::string &path) -> bool;
/**
 * @brief 删除空目录
 */
extern auto rm_empty_dir(const std::string &path) -> bool;
/**
 * 获取进程PID.
 */
extern auto get_pid() -> int;
/**
 * 获取当前调用堆栈信息.
 */
extern auto get_current_stack_trace_info(int depth = 64) -> std::string;
/**
 * 获取文件最近一次的修改时间.
 */
extern auto get_last_modify_time(const std::string &path) -> std::time_t;
/**
 * @brief 获取文件指定行及上下related_line的文件内容
 * @param file_path 文件路径
 * @param line 行号
 * @param related_line 需要附带显示的上下行数
 * @param [OUT] content 文件内容
 * @return true或false
 */
extern auto get_file_content(const std::string &file_path, int line,
                             int related_line, std::string &content) -> bool;

} // namespace util
} // namespace kratos

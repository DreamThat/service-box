#include "../box/service_logger.hh"
#include <sstream>

namespace kratos {
namespace service {

class ServiceBox;

/**
 * 模块日志实现.
 */
class ServiceLoggerImpl : public ServiceLogger {
  ServiceBox*       box_{nullptr};   ///< 服务容器
  std::stringstream ss_;             ///< 流式日志
  int               batch_level_{0}; ///< 当前的批量日志等级
  std::string       name_;           ///< 模块名

public:
  /**
   * 构造.
   *
   * \param name 模块名
   * \param box 服务容器
   */
  ServiceLoggerImpl(const std::string &name, ServiceBox *box);
  /**
   * 析构.
   *
   */
  virtual ~ServiceLoggerImpl();
  virtual auto verbose_log(const std::string &log) -> void override;
  virtual auto info_log(const std::string &log) -> void override;
  virtual auto debug_log(const std::string &log) -> void override;
  virtual auto warn_log(const std::string &log) -> void override;
  virtual auto except_log(const std::string &log) -> void override;
  virtual auto fail_log(const std::string &log) -> void override;
  virtual auto fatal_log(const std::string &log) -> void override;
  virtual auto verbose() -> ServiceLogger & override;
  virtual auto info() -> ServiceLogger & override;
  virtual auto debug() -> ServiceLogger & override;
  virtual auto warn() -> ServiceLogger & override;
  virtual auto except() -> ServiceLogger & override;
  virtual auto fail() -> ServiceLogger & override;
  virtual auto fatal() -> ServiceLogger & override;
  virtual auto operator<<(const char *str) -> ServiceLogger & override;
  virtual auto operator<<(const std::string &str) -> ServiceLogger & override;
  virtual auto operator<<(std::string &&str) -> ServiceLogger & override;
  virtual auto operator<<(std::int8_t i8) -> ServiceLogger & override;
  virtual auto operator<<(std::uint8_t ui8) -> ServiceLogger & override;
  virtual auto operator<<(std::int16_t i16) -> ServiceLogger & override;
  virtual auto operator<<(std::uint16_t ui16) -> ServiceLogger & override;
  virtual auto operator<<(std::int32_t i32) -> ServiceLogger & override;
  virtual auto operator<<(std::uint32_t ui32) -> ServiceLogger & override;
  virtual auto operator<<(std::int64_t i64) -> ServiceLogger & override;
  virtual auto operator<<(std::uint64_t ui64) -> ServiceLogger & override;
  virtual auto operator<<(float f) -> ServiceLogger & override;
  virtual auto operator<<(double d) -> ServiceLogger & override;
  virtual auto operator<<(const EndLog &) -> ServiceLogger & override;

public:
  /**
   * 批量流式日志起始.
   *
   * \param log_level 日志等级
   * \return
   */
  auto batch_log_begin(int log_level) -> void;
  /**
   * 批量流式日志结束，写入日志.
   *
   * \return
   */
  auto batch_log_end() -> void;
  /**
   * 获取日志流.
   *
   * \return
   */
  auto get_stream() -> std::ostream &;
  /**
   * 检测流是否有内容未写入.
   *
   * \return
   */
  auto stream_empty() -> bool;
  /**
   * 获取服务容器
   *
   * \return 服务容器
   */
  auto get_box()->ServiceBox*;
};

} // namespace service
} // namespace kratos

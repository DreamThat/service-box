import zipfile
import os
import platform
import sys
import subprocess
import ctypes
import shutil

class WindowsEnv:
    def __init__(self):
        pass

    def is_admin(self):
        try:
            return ctypes.windll.shell32.IsUserAnAdmin()
        except:
            return False

    def build(self):
        if platform.system() != "Windows":
            print("Only run on windows")
            return False
        if not self.is_admin():
            print("Need administrative priviledge")
            return False
        if os.system("java.exe -version") != 0:
            print("Please install JDK")
            return False
        if not os.path.exists('service-box-windows-dependency'):
            if os.system("git clone https://gitee.com/dennis-kk/service-box-windows-dependency") != 0:
                print("Pull windows dependency failed")
                return False
        zip = zipfile.ZipFile('./service-box-windows-dependency/service-box-windows-dependency.zip', 'r')
        for file in zip.namelist(): 
            zip.extract(file, "./service-box-windows-dependency/")
        zip.close()
        if not os.path.exists("C:\\Program Files (x86)\\protobuf"):
            shutil.copytree("./service-box-windows-dependency/protobuf", "C:\\Program Files (x86)\\protobuf")
        return True

if __name__ == "__main__":
    env = WindowsEnv()
    env.build()

        
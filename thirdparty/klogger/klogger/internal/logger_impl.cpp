/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "logger_impl.h"
#include "console_appender.h"
#include "file_appender.h"
#include "http_appender.h"
#include "tcp_appender.h"

LoggerImpl::LoggerImpl() {}

LoggerImpl::~LoggerImpl() {}

void LoggerImpl::destroy() {
  AppenderMap::iterator guard = _appMap.begin();
  for (; guard != _appMap.end();) {
    Appender *app = guard->second;
    guard++;        // destroy()会改变map内容
    app->destroy(); // 销毁添加器
  }
  // 销毁自己
  delete this;
}

Appender *LoggerImpl::getAppender(const std::string &name) {
  ScopeLock<RecursiveLock> guard(&_lock);
  AppenderMap::iterator found = _appMap.find(name);
  if (found == _appMap.end()) {
    return 0;
  }
  return found->second;
}

Appender *LoggerImpl::newAppender(const std::string &name,
                                  const std::string &attribute) {
  ScopeLock<RecursiveLock> guard(&_lock);
  if (attribute.find("file://") != std::string::npos) {
    if (_appMap.find(name) != _appMap.end()) {
      throw LoggerException("appender name exists");
    }
    FileAppender *fa = new FileAppender(this, name, attribute);
    _appMap[name] = fa;
    return fa;
  } else if (attribute.find("console://") != std::string::npos) {
    if (_appMap.find(name) != _appMap.end()) {
      throw LoggerException("appender name exists");
    }
    ConsoleAppender *ca = new ConsoleAppender(this, name, attribute);
    _appMap[name] = ca;
    return ca;
  } else if (attribute.find("http://") != std::string::npos) {
    if (_appMap.find(name) != _appMap.end()) {
      throw LoggerException("appender name exists");
    }
    HttpAppender *ha = new HttpAppender(this, name, attribute);
    _appMap[name] = ha;
    return ha;
  } else if (attribute.find("tcp://") != std::string::npos) {
    if (_appMap.find(name) != _appMap.end()) {
      throw LoggerException("appender name exists");
    }
    TcpAppender *ta = new TcpAppender(this, name, attribute);
    _appMap[name] = ta;
    return ta;
  } else {
      throw LoggerException("appender type not found");
  }
  return 0;
}

void LoggerImpl::remove(const std::string &name) {
  ScopeLock<RecursiveLock> guard(&_lock);
  _appMap.erase(_appMap.find(name));
}

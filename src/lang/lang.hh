#pragma once

#include <cstdarg>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <unordered_map>

namespace kratos {
namespace service {
class ServiceBox;
}
} // namespace kratos

namespace kratos {
namespace lang {

enum class LangID : std::uint64_t;

// 日志格式信息
struct LangInfo {
    std::string line; ///< 配置的日志
    std::string token; ///< 通配符, 用于校验
};
using DefaultMap = std::unordered_map<std::uint64_t, LangInfo>;

/**
 * 语言包.
 */
class Lang {
public:
  virtual ~Lang() {}
  /**
   * 加载默认语言包.
   * 
   * \param default_lang_path 语言包文件路径
   * \return true或false
   */
  virtual auto load_default(const std::string &default_lang_path) -> bool = 0;
  /**
   * 加载默认语言包.
   * 
   * \param name 语言包名字
   * \param default_map 语言包表
   * \return 
   */
  virtual auto load_default(const std::string& name, const DefaultMap & default_map)
      -> void = 0;
  /**
   * 获取当前语言.
   * 
   * \return 当前语言
   */
  virtual auto get_language() -> const std::string & = 0;
  /**
   * 获取语言ID对应的语言描述.
   * 
   * \param id 语言ID
   * \return 语言描述
   */
  virtual auto get_lang(LangID id) noexcept -> const std::string & = 0;
  /**
   * 加载语言包.
   * 
   * \param file_path 语言包路径
   * \return true或false
   */
  virtual auto load(const std::string &file_path) -> bool = 0;
  /**
   * 重新加载语言包.
   * 
   * \return true或false
   */
  virtual auto reload() -> bool = 0;
};

} // namespace lang
} // namespace kratos

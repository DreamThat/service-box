/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LEXER_H
#define LEXER_H

#include <string>
#include <list>

class Token;
class File;

/**
 * @brief lexer
 */
class Lexer {
public:
    /**
     * @brief ctor
     * @param source source code
     * @param file source file
     */
    Lexer(const std::string& source, File* file);

    /**
     * @brief dtor
     */
    ~Lexer();

    /**
     * @brief get next token
     */
    Token* getNext();

    /**
     * @brief get current token
     */
    Token* current();

private:
    /**
     * @brief check whether has next character in source stream
     */
    bool hasNext();

    /**
     * @brief get next character in source stream
     */
    int next();

    /**
     * @brief do new line
     */
    void newLine();

    /**
     * @brief check next N character
     */
    bool need(const char* literal, int length);

    /**
     * @brief do string token
     */
    Token* doString();

    /**
     * @brief do literal token
     */
    Token* doLiteral();

    /**
     * @brief do require token
     */
    Token* doRequire();

    /**
     * @brief do term token
     */
    Token* doTerm(int type, int fileId, int row, int column);

    /**
     * @brief skip comment
     */
    void skipComment();

private:
    std::list<Token*> _tokens;   // token list
    std::string       _source;   // source code
    Token*            _current;  // current token
    size_t            _loc;      // current location in source stream
    File*             _file;     // source file
    int               _row;      // current row
    int               _column;   // current column
    bool              _inString; // is do string?
};

#endif // LEXER_H

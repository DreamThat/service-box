/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sstream>

#include "../interface/config.h"

#include "array_impl.h"
#include "domain.h"
#include "file.h"
#include "file_manager.h"
#include "grammar.h"
#include "lexer.h"
#include "number_impl.h"
#include "string_impl.h"
#include "table_impl.h"
#include "token.h"

using namespace kconfig;

//
// chunk := domain*
// domain := '{' (require | statement | comment)* '}'
// require := 'require' literal
// statement := literal '=' (number | string | domain | array | table)
// comment := ';' (literal)*
// table := '<' pair (',' pair)* '>'
// pair := key ':' value
// key := literal
// value := array | table | string | number
// array := '(' (array | table | string | number)* ')'
// number := literal
// string := '"' literal '"'
//

Grammar::Grammar() {
  _root = new Domain("root", 0);
  _fileManager = new FileManager();

  _error = false;
  _base = "";
  _domain = _root;
  _lexer = 0;
  _file = 0;
  _lastError = "";
}

Grammar::~Grammar() {
  delete _root;
  delete _fileManager;

  _error = false;
  _base = "";
  _root = 0;
  _domain = 0;
  _lexer = 0;
  _file = 0;
  _fileManager = 0;
  _lastError = "";
}

bool Grammar::isOk() { return !_error; }

std::string Grammar::getLastError() {
  std::stringstream reason;

  if (0 != _file) {
    reason << _file->getPath();
  }

  if (0 != _lexer) {
    reason << "(" << _lexer->current()->getRow() << ":"
           << _lexer->current()->getColumn() << "):";
  }

  reason << _lastError;
  return reason.str();
}

void Grammar::error(const std::string &reason) {
  _error = true;
  _lastError = reason;
}

Domain *Grammar::parse(const std::string &base, const std::string &path) {
  _base = base;

  try {
    _file = _fileManager->newFile(_base + path, 0);
  } catch (std::runtime_error &) {
    ErrorReturnNull("file not found " + base + path);
  }

  _lexer = _file->getLexer();
  chunk();
  return _root;
}

void Grammar::chunk() {
  Token *token = 0;

  //
  // chunk := domain*
  //
  while ((0 != (token = _lexer->getNext())) && (!_error)) {
    if (token->type() == Token::REQUIRE) { // 'require'
      require();
    } else if (token->type() == Token::LITERAL) { // 'statement'
      statement();
    } else {
      if (_domain == _root) { // in top domain
        ErrorReturn("invalid token");
      }

      break;
    }
  }
}

void Grammar::require() {
  //
  // require := 'require' literal
  //
  Token *next = _lexer->getNext();

  if (0 == next) {
    ErrorReturn("need a file name");
  }

  if (next->type() != Token::LITERAL) {
    ErrorReturn("need a file name");
  }

  try {
    // open required file
    _file = _fileManager->newFile(_base + next->getText(), _file);
  } catch (std::runtime_error &) {
    ErrorReturn("file not found " + _base + next->getText());
  }

  // set current lexer
  _lexer = _file->getLexer();
  // do chunk
  chunk();

  if (_error) {
    return;
  }

  // up to parent file
  _file = _file->getParent();
  // up to parent lexer
  _lexer = _file->getLexer();
}

void Grammar::domain(const std::string &name) {
  //
  // domain := '{' (require | statement)* '}'
  //
  try {
    // open new domain
    _domain = _domain->newChild(name);
  } catch (std::runtime_error &exc) {
    ErrorReturn(exc.what());
  }

  // do chunk
  chunk();

  if (_error) {
    return;
  }

  Token *token = _lexer->current();

  if (0 == token) {
    ErrorReturn("need a '}'");
  }

  if (token->type() != Token::DOMAIN_END) {
    ErrorReturn("need a '}'");
  }

  // up to parent domain
  _domain = _domain->getParent();
}

void Grammar::array(Attribute *parent, const std::string &name) {
  //
  // array := '(' (array | table | string | number)* ')'
  //
  ArrayImpl *arrayImpl = new ArrayImpl();

  if (0 != parent) {
    if (parent->isArray()) {
      // push to array
      dynamic_cast<ArrayImpl *>(parent)->push(arrayImpl);
    } else if (parent->isTable()) {
      try {
        // insert to table
        dynamic_cast<TableImpl *>(parent)->add(name, arrayImpl);
      } catch (std::runtime_error &exc) {
        ErrorDeleteReturn(arrayImpl, exc.what());
      }
    }
  } else {
    try {
      // add to domain
      _domain->addAttribute(name, arrayImpl);
    } catch (std::runtime_error &exc) {
      ErrorDeleteReturn(arrayImpl, exc.what());
    }
  }

  //
  // do array element
  //
  Token *next = _lexer->getNext();

  while ((0 != next) && (next->type() != Token::ARRAY_END) && (!_error)) {
    if (next->type() == Token::ARRAY_START) {
      array(arrayImpl, ""); // 'array'
    } else if (next->type() == Token::TABLE_START) {
      table(arrayImpl, ""); // 'table'
    } else if (next->type() == Token::QUOTE) {
      string(arrayImpl, ""); // 'string'
    } else if (next->type() == Token::LITERAL) {
      number(arrayImpl, "", next->getText()); // 'number'
    } else {
      ErrorReturn("not a valid array element"); // un-supported element type
    }

    next = _lexer->getNext();

    if (0 == next) {
      ErrorReturn("need a ',' or ')'");
    }

    if ((next->type() != Token::COMMA) && (next->type() != Token::ARRAY_END)) {
      ErrorReturn("need a ',' or ')'");
    }

    if (next->type() == Token::ARRAY_END) {
      return;
    }

    // next token
    next = _lexer->getNext();
  }

  if (_error) {
    return;
  }

  if (next->type() != Token::ARRAY_END) {
    ErrorReturn("need a ')'");
  }
}

void Grammar::pair(Attribute *parent) {
  //
  // pair := key ':' value
  // key := literal
  // value := array | table | string | number
  //
  Token *token = _lexer->current();
  std::string key;

  if (token->type() == Token::LITERAL) {
    key = token->getText();
  } else {
    ErrorReturn("need a literal key for table pair");
  }

  token = _lexer->getNext();

  if (token->type() != Token::COLON) {
    ErrorReturn("need a ':'");
  }

  token = _lexer->getNext();

  if (token->type() == Token::LITERAL) {
    number(parent, key, token->getText()); // 'number'
  } else if (token->type() == Token::QUOTE) {
    string(parent, key); // 'string'
  } else if (token->type() == Token::ARRAY_START) {
    array(parent, key); // 'array'
  } else if (token->type() == Token::TABLE_START) {
    table(parent, key); // 'table'
  } else {
    ErrorReturn("not a valid table element");
  }
}

void Grammar::table(Attribute *parent, const std::string &name) {
  //
  // table := '<' pair (',' pair)* '>'
  //
  TableImpl *tableImpl = new TableImpl();

  if (0 != parent) {
    if (parent->isArray()) {
      dynamic_cast<ArrayImpl *>(parent)->push(tableImpl);
    } else if (parent->isTable()) {
      try {
        dynamic_cast<TableImpl *>(parent)->add(name, tableImpl);
      } catch (std::runtime_error &exc) {
        ErrorDeleteReturn(tableImpl, exc.what());
      }
    }
  } else {
    try {
      _domain->addAttribute(name, tableImpl);
    } catch (std::runtime_error &exc) {
      ErrorDeleteReturn(tableImpl, exc.what());
    }
  }

  Token *token = _lexer->getNext();

  while ((0 != token) && (token->type() != Token::TABLE_END) && (!_error)) {
    pair(tableImpl);
    token = _lexer->getNext();

    if (0 == token) {
      ErrorReturn("need a ',' or '>'");
    }

    if ((token->type() != Token::COMMA) &&
        (token->type() != Token::TABLE_END)) {
      ErrorReturn("need a ',' or '>'");
    }

    if (token->type() == Token::TABLE_END) {
      break;
    }

    token = _lexer->getNext();
  }

  if (_error) {
    return;
  }

  if (token->type() != Token::TABLE_END) {
    ErrorReturn("need a '>'");
  }
}

void Grammar::string(Attribute *parent, const std::string &key) {
  //
  // string := '"' literal '"'
  //
  Token *token = _lexer->getNext();

  if (0 == token) {
    ErrorReturn("need a string");
  }

  std::string value;

  if (token->type() == Token::LITERAL) {
    value = token->getText();
  } else {
    if (token->type() == Token::QUOTE) {
      value = "";
    } else {
      ErrorReturn("need a string");
    }
  }

  StringImpl *stringImpl = new StringImpl(value);

  if (0 != parent) {
    if (parent->isArray()) {
      dynamic_cast<ArrayImpl *>(parent)->push(stringImpl);
    } else if (parent->isTable()) {
      try {
        dynamic_cast<TableImpl *>(parent)->add(key, stringImpl);
      } catch (std::runtime_error &exc) {
        ErrorDeleteReturn(stringImpl, exc.what());
      }
    }
  } else {
    try {
      // add to domain
      _domain->addAttribute(key, stringImpl);
    } catch (std::runtime_error &exc) {
      ErrorDeleteReturn(stringImpl, exc.what());
    }
  }

  // empty string
  if (value.empty()) {
    return;
  }

  token = _lexer->getNext();

  if (0 == token) {
    ErrorReturn("need a '\"'");
  }

  if (token->type() != Token::QUOTE) {
    ErrorReturn("need a '\"'");
  }
}

void Grammar::number(Attribute *parent, const std::string &key,
                     const std::string &name) {
  //
  // number := literal
  //
  if (0 != parent) {
    if (parent->isArray()) {
      NumberImpl *numberImpl = new NumberImpl(name);
      dynamic_cast<ArrayImpl *>(parent)->push(numberImpl);
    } else if (parent->isTable()) {
      NumberImpl *numberImpl = new NumberImpl(name);

      try {
        dynamic_cast<TableImpl *>(parent)->add(key, numberImpl);
      } catch (std::runtime_error &exc) {
        ErrorDeleteReturn(numberImpl, exc.what());
      }
    } else {
      ErrorReturn("need a container");
    }
  } else {
    NumberImpl *numberImpl = new NumberImpl(name);

    try {
      _domain->addAttribute(key, numberImpl);
    } catch (std::runtime_error &exc) {
      ErrorDeleteReturn(numberImpl, exc.what());
    }
  }
}

void Grammar::statement() {
  //
  // statement := literal '=' (number | string | domain | array | table)
  //
  Token *current = _lexer->current();
  Token *next = _lexer->getNext();

  if (0 == next) {
    ErrorReturn("need a '='");
  }

  if (next->type() != Token::EQUAL) {
    ErrorReturn("need a '='");
  }

  next = _lexer->getNext();

  if (0 == next) {
    ErrorReturn("need a 'statement'");
  }

  if (next->type() == Token::LITERAL) {
    number(0, current->getText(), next->getText());
  } else if (next->type() == Token::QUOTE) {
    string(0, current->getText());
  } else if (next->type() == Token::DOMAIN_START) {
    domain(current->getText());
  } else if (next->type() == Token::ARRAY_START) {
    array(0, current->getText());
  } else if (next->type() == Token::TABLE_START) {
    table(0, current->getText());
  }
}

#include "os_service.hh"
#include <iostream>

#ifdef WIN32
#include "os_service_windows.hh"
#else
#endif // WIN32

#include "../util/os_util.hh"
#include "box_argument_impl.hh"

namespace kratos {
namespace service {

OperatingSystemServiceCtrl::OperatingSystemServiceCtrl() {}

OperatingSystemServiceCtrl::~OperatingSystemServiceCtrl() {}

#ifdef WIN32
auto OperatingSystemServiceCtrl::check_and_run(int argc, char **argv)
    -> OSCtrlStatus {
  argument::BoxArgumentImpl argument;
  if (!argument.parse(argc, (const char **)argv)) {
    if (argument.is_print_help()) {
      // 命令行解析失败，打印帮助
      std::cerr << argument.get_help_string();
    }
    return OSCtrlStatus::FAIL;
  }
  bool ret = true;
  // 无论是否已服务方式启动，都设置当前服务名
  set_service_name(util::get_binary_name());
  if (argument.get_command() == "install") {
    // 安装服务
    ret = install_service_internal();
  } else if (argument.get_command() == "uninstall") {
    // 卸载服务
    ret = uninstall_service_internal();
  } else if (argument.get_command() == "start_service") {
    // 启动服务
    ret = start_service();
  } else if (argument.get_command() == "stop_service") {
    // 关闭服务
    ret = stop_service();
  } else {
    if (!check_service()) {
      // 不是服务，退出
      return OSCtrlStatus::PASS;
    }
    // 设置当前工作路径
    ::SetCurrentDirectory(util::get_binary_path().c_str());
    // 按服务模式启动
    ret = start_as_service();
  }
  if (ret) {
    // 启动成功
    return OSCtrlStatus::OK;
  } else {
    // 启动失败
    return OSCtrlStatus::FAIL;
  }
}
#else
auto OperatingSystemServiceCtrl::check_and_run(int argc, char **argv)
    -> OSCtrlStatus {
  // Linux以daemon方式启动
  // TODO 支持systemctl
  return OSCtrlStatus::PASS;
}
#endif // WIN32

} // namespace service
} // namespace kratos

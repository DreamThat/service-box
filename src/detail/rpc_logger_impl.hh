#pragma once

#include "../repo/src/include/root/rpc_logger.h"
#include "../../thirdparty/klogger/klogger/interface/logger.h"

namespace kratos {
namespace service {

class ServiceBox;

/**
 * \see rpc::Logger
 *
 * 用于RPC框架使用外部日志添加器
 */
class RpcLoggerImpl : public rpc::Logger {
  ServiceBox *box_{nullptr};  ///< 服务容器

 public:
  /**
   * 构造
   * \param box ServiceBox指针
   */
  RpcLoggerImpl(ServiceBox *box);
  /**
   * 析构
   */
  virtual ~RpcLoggerImpl();
  /**
   * 写入日志
   * \param str 日志行
   */
  virtual void write(const std::string &str) override;
};

}
}


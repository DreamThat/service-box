// Machine generated code

#include <utility>
#include "rpc_singleton.h"
#include "object_pool.h"
#include "example.service.Login.impl.h"
#include "../../../src/include/example/Service/proxy/example.service.Service.proxy.h"
#include "rpc_root.h"
#include "../../../../../src/box/service_context.hh"

std::uint64_t user_id = 1;

LoginImpl::LoginImpl() {}
LoginImpl::~LoginImpl() {}

bool LoginImpl::onAfterFork(rpc::Rpc* rpc) {
    // 注册服务
    getContext()->register_service("/service/login");
    getContext()->info("LoginImpl初始化完成");
    return true;
}

bool LoginImpl::onBeforeDestory(rpc::Rpc* rpc) {
    // 反注册服务, 如果进程崩溃或者退出也会被反注册, 服务更新时也会被调用
    getContext()->unregister_service("/service/login");
    getContext()->info("LoginImpl清理完成");
    return true;
}

void LoginImpl::onTick(std::time_t ms) { return; }

std::uint64_t LoginImpl::login(rpc::StubCallPtr call, const std::string&, const std::string&) {
    return user_id++;
}

void LoginImpl::logout(rpc::StubCallPtr call, std::uint64_t) {
}

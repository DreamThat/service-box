#pragma once

#include <cstdint>
#include <sstream>
#include <string>
#include <unordered_map>
#include "../util/box_std_allocator.hh"

namespace kratos {
namespace service {

/**
 * 属性表.
 */
using ValueMap = PoolUnorederedMap<std::string, std::string>;

/**
 * 当前进程统计信息.
 */
class ProcStat {
public:
  /**
   * 析构.
   *
   */
  virtual ~ProcStat() {}
  /**
   * 获取进程统计信息属性.
   *
   * \param name 属性名
   * \param [OUT] value 值
   * \return true或false
   */
  template <typename T> bool get(const std::string &name, T &value) {
    std::string str_value;
    if (!get_value(name, str_value)) {
      return false;
    }
    std::istringstream iss;
    iss.str(str_value);
    try {
      iss >> value;
    } catch (...) {
      return false;
    }
    return true;
  }
  /**
   * 获取信息属性表.
   *
   * \param [OUT] value_map
   * \return true或false
   */
  virtual auto get(ValueMap &value_map) -> bool = 0;

protected:
  /**
   * 获取进程统计信息属性字符串值.
   *
   * \param name 属性名
   * \param value 字符串值
   * \return true或false
   */
  virtual auto get_value(const std::string &name,
    std::string &value) -> bool = 0;
};

} // namespace service
} // namespace kratos

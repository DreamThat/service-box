#include "../../src/detail/lang_impl.hh"
#include "../../src/util/os_util.hh"
#include "../framework/unittest.hh"
#include <fstream>

FIXTURE_BEGIN(test_lang)

using namespace kratos;

SETUP([]() {
  std::ofstream ofs;
  ofs.open("good.lang", std::ios::out | std::ios::trunc);
  ofs << "country test" << std::endl;
  ofs << "1 ��Ϣ" << std::endl;
  ofs.close();

  std::ofstream ofs1;
  ofs1.open("good1.lang", std::ios::out | std::ios::trunc);
  ofs1 << "country test" << std::endl;
  ofs1 << "2 hhh%s bb b" << std::endl;
  ofs1.close();

  std::ofstream ofs2;
  ofs2.open("wrong1.lang", std::ios::out | std::ios::trunc);
  ofs2 << "country test" << std::endl;
  ofs2 << "2 hhh%d b" << std::endl;
  ofs2.close();

  std::ofstream ofs3;
  ofs3.open("wrong2.lang", std::ios::out | std::ios::trunc);
  ofs3 << "country test" << std::endl;
  ofs3 << "2 hhh%s  b%s" << std::endl;
  ofs3.close();

  std::ofstream ofs4;
  ofs4.open("wrong3.lang", std::ios::out | std::ios::trunc);
  ofs4 << "country" << std::endl;
  ofs4 << "2 hhh%s  b%s" << std::endl;
  ofs4.close();

  std::ofstream ofs5;
  ofs5.open("wrong4.lang", std::ios::out | std::ios::trunc);
  ofs5 << "country test" << std::endl;
  ofs5 << "2" << std::endl;
  ofs5.close();
});

CASE(TestLang1) {
  lang::LangImpl lang;
  ASSERT_TRUE(lang.load("good.lang"));
  ASSERT_TRUE("test" == lang.get_language());
  ASSERT_TRUE(!lang.get_lang(lang::LangID::LANG_BOX_PARSE_ARGUMENT_FAIL).empty());
  ASSERT_TRUE(!lang.get_lang(lang::LangID::LANG_BOX_SERVICE_ADDRESS_INCORRECT).empty());
}

CASE(TestLang2) {
  lang::LangImpl lang;
  ASSERT_TRUE(lang.load("good1.lang"));
}

CASE(TestLang3) {
  lang::LangImpl lang;
  ASSERT_TRUE(!lang.load("wrong1.lang"));
}

CASE(TestLang4) {
  lang::LangImpl lang;
  ASSERT_TRUE(!lang.load("wrong2.lang"));
}

CASE(TestLang5) {
  lang::LangImpl lang;
  ASSERT_TRUE(!lang.load("wrong3.lang"));
}

CASE(TestLang6) {
  lang::LangImpl lang;
  ASSERT_TRUE(!lang.load("wrong4.lang"));
}

CASE(TestLang7) {
    lang::LangImpl lang;
    ASSERT_TRUE(!lang.get_language().empty());
}

CASE(TestLang8) {
    lang::LangImpl lang;
    ASSERT_TRUE(lang.load("good1.lang"));
    ASSERT_TRUE(lang.reload());
}

TEARDOWN([]() {
  util::remove_file("good.lang");
  util::remove_file("good1.lang");
  util::remove_file("wrong1.lang");
  util::remove_file("wrong2.lang");
  util::remove_file("wrong3.lang");
  util::remove_file("wrong4.lang");
});

FIXTURE_END(test_lang)

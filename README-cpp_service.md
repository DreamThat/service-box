# 编写C++语言服务

C++服务已DLL/SO方式进行部署, 这个DLL/SO称之为bundle(这个名称借鉴于OSGi, 但完全不能类比), 每一个bundle内含有对一个服务接口的实现。

## 编写服务定义

假设有一个服务定义, 定义在文件名为example.idl（接口描述文件请参考:[IDL服务定义](https://gitee.com/dennis-kk/rpc-frontend/blob/master/README.md)）内，example.idl存放于src/repo/example目录内:


```
struct Dummy {
    i32 field1
    string field2
    seq<string> field3
    set<string> field4
    dict<i64,string> field5
    bool field6
    float field7
    double field8
}

struct Data {
    i32 field1
    string field2
    seq<string> field3
    set<string> field4
    dict<i64,string> field5
    bool field6
    float field7
    double field8    
    Dummy field9
    seq<Dummy> field10
    set<string> field11
    dict<i64,Dummy> field12
}

service dynamic ServiceDynamic multiple=16 {
    oneway void method1(Data,string) 
    string method2(i8,set<string>,ui64) timeout=2000 retry = 3
    dict<i64,Dummy> method3(i8,set<string>,dict<i64,string>, ui64)
    void method4(void)
    void method5()
}
```

服务名为ServiceDynamic,有5个方法。

## 生成C++服务

在src/repo目录下运行:

```
python repo.py -t cpp -b example:ServiceDynamic
```

## 实现功能

在src/repo/usr/impl/example/ServiceDynamic目录下, 是ServiceDynamic服务的实现文件。

1. [头文件](https://gitee.com/dennis-kk/service-box/blob/master/src/repo/usr/impl/example/ServiceDynamic/example.service.ServiceDynamic.impl.h)
2. [实现文件](https://gitee.com/dennis-kk/service-box/blob/master/src/repo/usr/impl/example/ServiceDynamic/example.service.ServiceDynamic.impl.cpp)

## 修改并重新生成服务

打开src/repo/idl/example.idl，修改完毕后在src/repo下运行:

```
python repo.py -t cpp -u example:ServiceDynamic
python repo.py -t cpp -b example:ServiceDynamic
```





#pragma once

#include "box_console.hh"
#include "../util/box_std_allocator.hh"
#include <unordered_set>
#include <string>

namespace kratos {
namespace console {

class BoxConsoleImpl : public BoxConsole {
  using NameSet = kratos::service::PoolUnorderedSet<std::string>;

  NameSet     switch_set_;       ///< 已注册的开关名字集合
  NameSet     selection_set_;    ///< 已注册的选项名字集合
  Console*    console_{nullptr}; ///< 控制台
  std::string name_;             ///< 模块名

public:
  /**
   * 构造
   * 
   * \param console 控制台实例
   */
  BoxConsoleImpl(const std::string& name, Console* console);
  /**
   * 析构
   */
  virtual ~BoxConsoleImpl();
  /**
   * 添加控制台开关plugin
   *
   * \param name 开关名字
   * \param switch_plugin plugin
   */
  virtual auto add_switch(const std::string& name,
      ConsoleSwitchPlugin switch_plugin)->void override;
  /**
   * 移除控制台开关plugin
   *
   * \param name 开关名字
   */
  virtual auto remove_switch(const std::string& name)->void override;
  /**
   * 添加控制台选项plugin
   *
   * \param name 选项名字
   * \param selection_plugin plugin
   */
  virtual auto add_selection(const std::string& name,
      ConsoleSelectionPlugin selection_plugin)->void override;
  /**
   * 移除控制台选项plugin
   *
   * \param name 选项名字
   */
  virtual auto remove_selection(const std::string& name)->void override;
};

}
}
